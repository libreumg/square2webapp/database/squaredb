\i sql/appconfig/enums/role.sql
alter table appconfig.t_menurole alter column rolename type appconfig.enum_role using rolename::appconfig.enum_role;
