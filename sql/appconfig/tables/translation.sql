create table appconfig.t_translation (
	lastchange timestamp null default current_timestamp,
	pk int not null generated always as identity primary key,
	fk_menuitem int not null references appconfig.t_menuitem(pk),
	locale appconfig.enum_locale not null,
	category appconfig.enum_category not null,
	value text,
	unique(fk_menuitem, locale, category)
);

create index on appconfig.t_translation(fk_menuitem);

grant select, insert, update, delete on appconfig.t_translation to interface_square;
