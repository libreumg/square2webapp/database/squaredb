create table appconfig.t_menuitem (
	lastchange timestamp null default current_timestamp,
	pk int not null generated always as identity primary key,
	id text not null unique,
	order_nr int not null unique,
	url text not null
);

create index on appconfig.t_menuitem(id);

grant select, insert, update, delete on appconfig.t_menuitem to interface_square;
