create table appconfig.t_menurole (
	lastchange timestamp null default current_timestamp,
	pk int not null generated always as identity primary key,
	fk_menuitem int not null references appconfig.t_menuitem(pk),
	rolename appconfig.enum_role not null,
	unique(fk_menuitem, rolename)
);

create index on appconfig.t_menurole(fk_menuitem);

grant select, insert, update, delete on appconfig.t_menurole to interface_square;

