create view appconfig.v_menuitem as
select m.pk, m.id, m.order_nr, m.url, tn.value as name, td.value as description, tn.locale
from appconfig.t_menuitem m
left join appconfig.t_translation tn on tn.fk_menuitem = m.pk and tn.category = 'name'
left join appconfig.t_translation td on td.fk_menuitem = m.pk and td.category = 'description' and tn.locale = td.locale;

grant select on appconfig.v_menuitem to interface_square;
