create or replace function hist_square.history() returns trigger security definer as $$
declare
  found boolean;
  historytablename text;
  historyschema text;
  historytable text;
  originaltable text;
  inet_client_addr text;
  columnNames text;
begin
	-- defined: all tables start with t_
  historytablename := 'h_' || substr(tg_table_name, 3);
  -- defined: all history schemata are named by hist_ + schema name 
  historyschema := 'hist_' || tg_table_schema;
 
  historytable := historyschema || '.' || historytablename;
  originaltable := tg_table_schema || '.' || tg_table_name;
  
  -- check for history table existence
  select count(1) > 0 into found from information_schema.tables where table_schema = historyschema and table_name = historytablename;

  -- create history table if needed
  if not found then
    raise notice 'create history table % for table %', historytable, originaltable;
  
    execute 'create table ' || historytable || ' as select null::text dml_operation, null::timestamp as historydate, null::text as sessionuser, null::text as remote_ip, * from ' || originaltable || ' limit 0';
    execute 'alter table ' || historytable || ' alter column historydate set default now(), alter column dml_operation set not null, alter column sessionuser set not null, alter column remote_ip set not null';
    execute 'grant select on ' || historytable || ' to interface_square';
  end if;
	
  -- find column names of original table
  select string_agg(column_name, ',' order by ordinal_position) into columnNames from information_schema.columns where table_schema = tg_table_schema and table_name = tg_table_name;
  
  -- fill history table
  select coalesce(inet_client_addr()::text, 'localhost') into inet_client_addr;
  execute 'insert into ' || historytable || '(dml_operation, historydate, sessionuser, remote_ip,' || columnNames ||
    ') values (''' || lower(tg_op) || ''', now(), ''' || session_user || ''',''' || inet_client_addr || ''', ($1).*)' using old;
  
  -- reset lastchange on original table if needed
  if tg_op = 'UPDATE' then
    select now() into new.lastchange;
    return new;
  else
    return old;                                                                                                                                                        
  end if;                                                                                                                                                              
end                                                                                                                                                                    
$$ language plpgsql;

grant execute on function hist_square.history() to interface_square, interface_rserver;
