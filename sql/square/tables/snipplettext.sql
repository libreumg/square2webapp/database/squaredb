create table if not exists square.T_SNIPPLETTEXT(
  lastchange timestamp default CURRENT_TIMESTAMP,
  pk serial NOT NULL,
  fk_snipplet integer NOT NULL,
  fixed boolean default true NOT NULL,
  constraint pk_snipplettext primary key (pk),
  constraint fk_snipplettext_snipplet foreign key (fk_snipplet) references square.t_snipplet(pk)
);

comment on column square.t_snipplettext.fk_snipplet is 'corresponding snipplet';
comment on column square.t_snipplettext.fixed is 'can this snipplet be edited in report module?';

grant select,insert,update,delete on square.T_SNIPPLETTEXT to INTERFACE_SQUARE;
grant select,insert,update,delete on square.T_SNIPPLETTEXT to INTERFACE_RSERVER;
grant usage on square.T_SNIPPLETTEXT_pk_seq to INTERFACE_SQUARE;
grant usage on square.T_SNIPPLETTEXT_pk_seq to INTERFACE_RSERVER;
