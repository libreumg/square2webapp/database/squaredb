create table if not exists square.t_release(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_element integer not null,
  name text not null,
  shortname text,
  description text,
  rolling boolean default true not null,
  releasedate timestamp,
  location text not null,
  backup boolean default false not null,
  fk_privilege integer not null,
  active boolean default true not null,
  mergecolumn integer not null,
  constraint pk_qs_release primary key (pk),
  constraint uk_release unique (name),
  constraint fk_release_element foreign key (fk_element) references square.t_element(pk),
  constraint fk_release_privilege foreign key (fk_privilege) references square.t_privilege(pk),
  constraint fk_release_mergecolumn foreign key (mergecolumn) references square.t_element(pk)
);

comment on column square.t_release.pk is 'primary key';
comment on column square.t_release.fk_element is 'reference to study';
comment on column square.t_release.name is 'unique name of release';
comment on column square.t_release.shortname is 'short name of release for references';
comment on column square.t_release.description is 'description of release';
comment on column square.t_release.rolling is 'type of release; false = final, true = rolling (default)';
comment on column square.t_release.releasedate is 'date of release, may be null';
comment on column square.t_release.location is 'location of data containers (database schema name)';
comment on column square.t_release.backup is 'default false';
comment on column square.t_release.active is 'default true';
comment on column square.t_release.mergecolumn is 'id of data set to merge tables by';

grant select,insert,update,delete on square.t_release to interface_square;
grant select on square.t_release to interface_rserver;

