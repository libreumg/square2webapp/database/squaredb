create table if not exists square.t_widgetbased(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  key text not null,
  value jsonb,
  fk_functioninputtype integer not null,
  type_restriction jsonb,
  order_nr integer not null,
  constraint pk_widgetbased primary key (pk),
  constraint uk_widgetbased unique (key),
  constraint fk_widgetbased_fit foreign key (fk_functioninputtype) references square.t_functioninputtype(pk)
);

-- create trigger trg_bud_widgetbased before update or delete on square.t_widgetbased for each row execute procedure hist_square.history();

comment on column square.t_widgetbased.key is 'unique key';
comment on column square.t_widgetbased.value is 'the value';

grant select,insert,update,delete on square.t_widgetbased to interface_square;
grant select on square.t_widgetbased to interface_rserver;

