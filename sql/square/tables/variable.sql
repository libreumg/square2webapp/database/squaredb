create table if not exists square.t_variable(
  lastchange timestamp default current_timestamp,
  fk_element integer not null,
  column_name text not null,
  fk_scale enum_scale not null,
  var_order integer,
  optional boolean default false not null,
  control_type enum_controlvar,
  valuelist text,
  fk_variableusage integer,
  datatype enum_datatype not null,
  idvariable boolean not null default false,
  constraint pk_variable primary key (fk_element),
  constraint fk_variable_elem foreign key (fk_element) references square.t_hierarchy(key_element),
  constraint fk_variable_usage foreign key (fk_variableusage) references square.t_variableusage(pk)
);

-- create trigger trg_bud_variable before update or delete on square.t_variable for each row execute procedure hist_square.history();

comment on column square.t_variable.fk_element is 'unique id';
comment on column square.t_variable.column_name is 'column name of element in a release table';
comment on column square.t_variable.fk_scale is 'scale; use this until t_core_variable is finished';
comment on column square.t_variable.var_order is 'order';
comment on column square.t_variable.optional is 'if this variable is optional';
comment on column square.t_variable.control_type is 'type of this variable';
comment on column square.t_variable.valuelist is 'values; definition of structure may vary and depends on r';
comment on column square.t_variable.fk_variableusage is 'usage of variable';
comment on column square.t_variable.datatype is 'dataquieR datatype';

grant select on square.t_variable to interface_rserver;
grant select,insert,update,delete on square.t_variable to interface_square;
grant select,insert,update,delete on square.t_variable to interface_rserver;



