create table if not exists square.t_matrixelementvalue(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_report integer not null,
  fk_matrixcolumn integer not null,
  fk_variablegroupelement integer not null,
  constraint pk_matrixelementvalue primary key (pk),
  constraint uk_matrixelementvalue unique (fk_report, fk_matrixcolumn, fk_variablegroupelement),
  constraint fk_matrixelementvalue_report foreign key (fk_report) references square.t_report(pk),
  constraint fk_matrixelementvalue_elem foreign key (fk_variablegroupelement) references square.t_variablegroupelement(pk),
  constraint fk_matrixelementvalue_matcol foreign key (fk_matrixcolumn) references square.t_matrixcolumn(pk)
);

-- create trigger trg_bud_matrixelementvalue before update or delete on square.t_matrixelementvalue for each row execute procedure hist_square.history();

comment on column square.t_matrixelementvalue.fk_report is 'id of analysis matrix report';
comment on column square.t_matrixelementvalue.fk_matrixcolumn is 'id of analysis function';
comment on column square.t_matrixelementvalue.fk_variablegroupelement is 'id of variablegroupelement';

grant select,insert,update,delete on square.t_matrixelementvalue to INTERFACE_SQUARE;
grant select on square.t_matrixelementvalue to INTERFACE_RSERVER;

