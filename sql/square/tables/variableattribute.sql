create table if not exists square.t_variableattribute(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_variable integer not null,
  name text not null,
  value text,
  fk_role int not null,
  is_primary_attribute boolean not null default false,
  locale text,
  constraint pk_variableattribute primary key (pk),
  constraint uk_variableattribute unique (fk_variable, name),
  constraint fk_variableattribute_variable foreign key (fk_variable) references square.t_variable(fk_element),
  constraint fk_variableattribute_role foreign key (fk_role) references square.t_variableattributerole(pk)
);

-- create trigger trg_bud_variableattribute before update or delete on square.t_variableattribute for each row execute procedure hist_square.history();

comment on column square.t_variableattribute.pk is 'unique id';
comment on column square.t_variableattribute.fk_variable is 'reference to variable';
comment on column square.t_variableattribute.name is 'name of attribute';
comment on column square.t_variableattribute.value is 'value of attribute';
comment on column square.t_variableattribute.role is 'role of variable attribute';

grant select,insert,update,delete on square.t_variableattribute to interface_square;
grant select,insert,update,delete on square.t_variableattribute to interface_rserver;

