create table if not exists square.t_latexdef(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  lightcolor text default '0.5, 0.5, 0.5' not null,
  darkcolor text default '0.9, 0.9, 0.9' not null,
  logo text not null,
  constraint pk_latexdef primary key (pk),
  constraint uk_latexdef unique (name)
);

comment on column square.t_latexdef.pk is 'primary key';
comment on column square.t_latexdef.name is 'unique name of definition';
comment on column square.t_latexdef.lightcolor is 'latex color code of light color';
comment on column square.t_latexdef.darkcolor is 'latex code of dark color';
comment on column square.t_latexdef.logo is 'latex code of logo (best tikz environment)';

grant select,insert,update,delete on square.t_latexdef to INTERFACE_SQUARE;

