create table if not exists square.t_functionoutput(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_function integer not null,
  name text not null,
  description text not null,
  type integer not null,
  constraint pk_functionoutput primary key (pk),
  constraint uk_functionoutput unique (fk_function, name),
  constraint fk_functionoutput_function foreign key (fk_function) references square.t_function(pk),
  constraint fk_functionputput_type foreign key (type) references square.t_functionoutputtype(pk)
);

comment on column square.t_functionoutput.pk is 'primary key';
comment on column square.t_functionoutput.fk_function is 'id of function';
comment on column square.t_functionoutput.name is 'name of output param';
comment on column square.t_functionoutput.description is 'meaning';

grant select,insert,update,delete on square.t_functionoutput to INTERFACE_SQUARE;
grant select,insert,update on square.t_functionoutput to interface_rserver;

