create table if not exists square.t_missing(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_missinglist integer not null,
  code text not null,
  name_in_study text,
  fk_missingtype integer not null,
  constraint pk_missing primary key (pk),
  constraint uk_missing unique (fk_missinglist, code),
  constraint fk_missing_missinglist foreign key (fk_missinglist) references square.t_missinglist(pk),
  constraint fk_missing_missingtype foreign key (fk_missingtype) references square.t_missingtype(pk)
);

-- create trigger trg_bud_missing before update or delete on square.t_missing for each row execute procedure hist_square.history();

comment on column square.t_missing.pk is 'primary key';
comment on column square.t_missing.fk_missinglist is 'reference to missing list';
comment on column square.t_missing.code is 'missing code';
comment on column square.t_missing.name_in_study is 'missing name in study';
comment on column square.t_missing.fk_missingtype is 'reference to missing type';

grant select,insert,update,delete on square.t_missing to INTERFACE_SQUARE;
grant select,insert,update on square.t_missing to interface_rserver;
