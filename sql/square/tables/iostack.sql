create table if not exists square.t_iostack(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  filetype text,
  interpretation text,
  db_version int not null,
  smi_version text not null,
  fk_usnr int not null,
  constraint pk_iostack primary key (pk),
  constraint fk_iostack_usnr foreign key (fk_usnr) references square.t_person(usnr)
);

-- create trigger trg_bud_iostack before update or delete on square.t_iostack for each row execute procedure hist_square.history();

comment on column square.t_iostack.filetype is 'type from the R package squaremetadataimporter (such as zip, xls, ods, csv, ...)';
comment on column square.t_iostack.interpretation is 'interpretation from the R package squaremetadataimporter (such like context vars, variable attributes, ...)';
comment on column square.t_iostack.db_version is 'version of the database (see v_version.db_version)';
comment on column square.t_iostack.smi_version is 'version of squaremetadataimporter';

grant select,insert,delete on square.t_iostack to interface_square;
grant select,update on square.t_iostack to interface_rserver;

