create table if not exists square.t_variablegroupelement(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_variablegroup integer not null,
  fk_element integer not null,
  varorder integer not null,
  constraint pk_variablegroupelement primary key (pk),
  constraint uk_variablegroupelement unique (fk_variablegroup, fk_element),
  constraint fk_variablegroupelement_vg foreign key (fk_variablegroup) references square.t_variablegroupdef(pk),
  constraint fk_variablegroupelement_var foreign key (fk_element) references square.t_element(pk)
);

create trigger trg_bud_variablegroupelement before update or delete on square.t_variablegroupelement for each row execute procedure hist_square.history();

comment on column square.t_variablegroupelement.fk_variablegroup is 'reference to variable group';
comment on column square.t_variablegroupelement.fk_element is 'id of variable';
comment on column square.t_variablegroupelement.varorder is 'order inside this group';

grant select,insert,update,delete on square.t_variablegroupelement to INTERFACE_SQUARE;
grant select on square.t_variablegroupelement to interface_rserver;

