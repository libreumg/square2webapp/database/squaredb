create table if not exists square.t_usergroup(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_usnr integer not null,
  fk_group integer not null,
  constraint pk_usergroup primary key (pk),
  constraint uk_usergroup unique (fk_usnr, fk_group),
  constraint fk_usergroup_usnr foreign key (fk_usnr) references square.t_person(usnr),
  constraint fk_usergroup_group foreign key (fk_group) references square.t_group(pk)
);

create index idx_usergroup_usnr on square.t_usergroup(fk_usnr);
create index idx_usergroup_group on square.t_usergroup(fk_group);

comment on column square.t_usergroup.pk is 'unique id';
comment on column square.t_usergroup.fk_usnr is 'reference to user';
comment on column square.t_usergroup.fk_group is 'reference to group';

grant select,insert,update,delete on square.t_usergroup to interface_square;

