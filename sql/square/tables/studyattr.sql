create table if not exists square.t_studyattr(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_study int not null,
  fk_attrtype int not null,
  value jsonb not null,
  constraint pk_studyattr primary key (pk),
  constraint uk_studyattr unique (fk_study, fk_attrtype),
  constraint fk_studyattr_study foreign key (fk_study) references square.t_study(pk),
  constraint fk_studyattr_attrtype foreign key (fk_attrtype) references square.t_attrtype(pk)
);

-- create trigger trg_bud_studyattr before update or delete on square.t_studyattr for each row execute procedure hist_square.history();


grant select,insert,update,delete on square.t_studyattr to interface_square;
grant select,insert,update,delete on square.t_studyattr to interface_rserver;

