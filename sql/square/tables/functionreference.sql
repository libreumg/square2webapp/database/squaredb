create table if not exists square.t_functionreference(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_function integer not null,
  required_function integer not null,
  constraint pk_functionreference primary key (pk),
  constraint uk_functionreference unique (fk_function, required_function),
  constraint fk_functionreference_function foreign key (fk_function) references square.t_function(pk),
  constraint fk_functionreference_req foreign key (required_function) references square.t_function(pk)
);

create index idx_functionreference_function on square.t_functionreference(fk_function);

comment on column square.t_functionreference.pk is 'primary key';
comment on column square.t_functionreference.fk_function is 'id of function';
comment on column square.t_functionreference.required_function is 'id of required function';

grant select,insert,update,delete on square.t_functionreference to INTERFACE_SQUARE;
grant select on square.t_functionreference to interface_rserver;

