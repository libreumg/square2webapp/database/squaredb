create table if not exists square.t_attrtype(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  fk_widgetbased int,
  entity square.enum_entity not null,
  meta jsonb,
  constraint pk_attrtype primary key (pk),
  constraint uk_attrtype unique (name),
  constraint fk_attrtype_widgetbases foreign key (fk_widgetbased) references square.t_widgetbased(pk)
);

-- create trigger trg_bud_attrtype before update or delete on square.t_attrtype for each row execute procedure hist_square.history();


grant select,insert,update,delete on square.t_attrtype to interface_square;
grant select,insert,update,delete on square.t_attrtype to interface_rserver;

