create table if not exists square.t_userprivilege(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_privilege integer not null,
  fk_usnr integer not null,
  square_acl enum_accesslevel,
  constraint pk_userprivilege primary key (pk),
  constraint uk_userprivilege unique (fk_usnr, fk_privilege),
  constraint fk_userprivilege_privilege foreign key (fk_privilege) references square.t_privilege(pk),
  constraint fk_userprivilege_usnr foreign key (fk_usnr) references square.t_person(usnr)
);

create index idx_userprivilege_privilege on square.t_userprivilege(fk_privilege);
create index idx_userprivilege_usnr on square.t_userprivilege(fk_usnr);


grant select,insert,update,delete on square.t_userprivilege to INTERFACE_SQUARE;
grant select,insert,update,delete on square.t_userprivilege to interface_rserver;

