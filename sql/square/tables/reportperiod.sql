create table if not exists square.t_reportperiod(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_report integer not null,
  date_from timestamp not null,
  date_until timestamp not null,
  constraint pk_reportperiod primary key (pk),
  constraint uk_reportperiod unique (fk_report, date_from, date_until),
  constraint fk_reportperiod_report foreign key (fk_report) references square.t_report(pk)
);

-- create trigger trg_bud_reportperiod before update or delete on square.t_reportperiod for each row execute procedure hist_square.history();

comment on column square.t_reportperiod.fk_report is 'reference to report';
comment on column square.t_reportperiod.date_from is 'interval start';
comment on column square.t_reportperiod.date_until is 'interval end';

grant select,insert,update,delete on square.t_reportperiod to INTERFACE_SQUARE;

