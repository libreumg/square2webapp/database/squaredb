create table if not exists square.t_missingtype(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  fk_parent integer,
  constraint pk_missingtype primary key (pk),
  constraint uk_missingtype unique (name),
  constraint fk_missingtype foreign key (fk_parent) references square.t_missingtype(pk)
);

-- create trigger trg_bud_missingtype before update or delete on square.t_missingtype for each row execute procedure hist_square.history();

comment on column square.t_missingtype.pk is 'primary key';
comment on column square.t_missingtype.name is 'name of missing list';
comment on column square.t_missingtype.fk_parent is 'parent of missing list';

grant select,insert,update,delete on square.t_missingtype to INTERFACE_SQUARE;
grant select,insert,update on square.t_missingtype to interface_rserver;

