create table if not exists square.t_languagekey(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text,
  fk_privilege integer,
  constraint pk_languagekey primary key (pk),
  constraint uk_languagekey unique (name),
  constraint fk_languagekey_privilege foreign key (fk_privilege) references square.t_privilege(pk)
);

comment on column square.t_languagekey.name is 'language key';
comment on column square.t_languagekey.fk_privilege is 'right for this key';

grant select,insert,update,delete on square.t_languagekey to interface_square;
grant select,insert,update on square.t_languagekey to interface_rserver;
