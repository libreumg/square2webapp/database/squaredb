create table if not exists square.t_element(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_parent integer,
  translation jsonb,
  fk_privilege integer,
  name text,
  order_nr integer,
  unique_name text not null,
  fk_missinglist integer,
  constraint pk_element primary key (pk),
  constraint uk_element_un unique (unique_name),
  constraint fk_element_parent foreign key (fk_parent) references square.t_element(pk),
  constraint fk_element_lang foreign key (fk_lang) references square.t_languagekey(pk),
  constraint fk_element_privilege foreign key (fk_privilege) references square.t_privilege(pk),
  constraint fk_element_missinglist foreign key (fk_missinglist) references square.t_missinglist(pk)
);

create index idx_element_parent on square.t_element(fk_parent);

create trigger trg_bud_element before update or delete on square.t_element for each row execute procedure hist_square.history();

create trigger trg_biu_element before insert or update on t_element for each row execute procedure square.check_jsonb_translation();

comment on column square.t_element.pk is 'unique element id';
comment on column square.t_element.fk_parent is 'reference to element id of parent element';
comment on column square.t_element.translation is 'translation in jsonb format {isocode1: translation1, ...}';
comment on column square.t_element.fk_privilege is 'privilege for this key';
comment on column square.t_element.name is 'name of variable';
comment on column square.t_element.order_nr is 'order in hierarchy on same level';
comment on column square.t_element.unique_name is 'unique name';
comment on column square.t_element.fk_missinglist is 'missing list on this section';

grant select,insert,update,delete on square.t_element to interface_square;
grant select,insert,update,delete on square.t_element to interface_rserver;

