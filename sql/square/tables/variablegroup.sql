create table if not exists square.t_variablegroup(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_variablegroupdef integer not null,
  fk_element integer not null,
  varorder integer not null,
  constraint pk_variablegroup primary key (pk),
  constraint uk_variablegroup unique (fk_variablegroupdef, fk_element),
  constraint fk_variablegroup_def foreign key (fk_variablegroupdef) references square.t_variablegroupdef(pk),
  constraint fk_variablegroup_var foreign key (fk_element) references square.t_hierarchy(key_element)
);

comment on column square.t_variablegroup.fk_variablegroupdef is 'unique id';
comment on column square.t_variablegroup.fk_element is 'id of variable';
comment on column square.t_variablegroup.varorder is 'order inside this group';

grant select,insert,update,delete on square.t_variablegroup to INTERFACE_SQUARE;
grant select on square.t_variablegroup to interface_rserver;

