create table if not exists square.t_variablegroupdef(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  description text not null,
  parent_variablegroup integer,
  fk_privilege integer not null,
  constraint pk_variablegroupdef primary key (pk),
  constraint fk_variablegroupdef_parent foreign key (parent_variablegroup) references square.t_variablegroupdef(pk),
  constraint fk_variablegroup_privilege foreign key (fk_privilege) references square.t_privilege(pk)
);

create index idx_variablegroupdef_parent on square.t_variablegroupdef(parent_variablegroup);

comment on column square.t_variablegroupdef.name is 'name of this variable group';
comment on column square.t_variablegroupdef.description is 'description for this variable group';
comment on column square.t_variablegroupdef.parent_variablegroup is 'optional parent variablegroup';
comment on column square.t_variablegroupdef.fk_privilege is 'privilege on this variable group';

grant select,insert,update,delete on square.t_variablegroupdef to INTERFACE_SQUARE;

