create table if not exists square.t_lastchange(
  lastchange timestamp default current_timestamp,
  schemaname text not null,
  tablename text not null,
  constraint uk_lastchange unique (schemaname, tablename)
);


grant select on square.t_lastchange to interface_square;

