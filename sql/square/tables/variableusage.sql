create table if not exists square.t_variableusage(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  fk_lang integer not null,
  constraint pk_variableusage primary key (pk),
  constraint uk_variableusage unique (name),
  constraint fk_variableusage_lang foreign key (fk_lang) references square.t_languagekey(pk)
);

-- create trigger trg_bud_variableusage before update or delete on square.t_variableusage for each row execute procedure hist_square.history();

comment on column square.t_variableusage.name is 'label';
comment on column square.t_variableusage.fk_lang is 'translation key';

grant select on square.t_variableusage to interface_rserver;
grant select,insert,update,delete on square.t_variableusage to interface_square;

