create table if not exists square.t_matrixcolumnparameter(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_matrixcolumn integer not null,
  fk_functioninput integer not null,
  fk_report integer,
  value text,
  fixed enum_anadef not null,
  refers_metadata boolean default false not null,
  constraint pk_matrixcolumnparameter primary key (pk),
  constraint fk_matrixcolumnparameter_matrixcolumn foreign key (fk_matrixcolumn) references square.t_matrixcolumn(pk),
  constraint fk_matrixcolumnparameter_functioninput foreign key (fk_functioninput) references square.t_functioninput(pk),
  constraint fk_matrixcolumnparameter_report foreign key (fk_report) references square.t_report(pk)
);

create unique index idxu_matrixcolumnparameter_repnull on square.t_matrixcolumnparameter (fk_matrixcolumn, fk_functioninput) where fk_report is null;
create unique index idxu_matrixcolumnparameter_repnotnull on square.t_matrixcolumnparameter (fk_matrixcolumn, fk_functioninput, fk_report) where fk_report is not null;

-- create trigger trg_bud_matrixcolumnparameter before update or delete on square.t_matrixcolumnparameter for each row execute procedure hist_square.history();

comment on column square.t_matrixcolumnparameter.value is 'value of input parameter';

grant select,insert,update,delete on square.t_matrixcolumnparameter to INTERFACE_SQUARE;
grant select on square.t_matrixcolumnparameter to INTERFACE_RSERVER;

