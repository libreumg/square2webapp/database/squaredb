create table if not exists square.t_functioninput(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_function integer not null,
  input_name text not null,
  default_value text,
  ordering integer not null,
  description text not null,
  fk_type integer not null,
  type_restriction jsonb,
  refers_metadata boolean default false not null,
  constraint pk_functioninput primary key (pk),
  constraint uk_functioninput unique (fk_function, input_name),
  constraint fk_functioninput_function foreign key (fk_function) references square.t_function(pk),
  constraint fk_functioninput_type foreign key (fk_type) references square.t_functioninputtype(pk)
);

create index idx_functioninput_function on square.t_functioninput(fk_function);

comment on column square.t_functioninput.pk is 'primary key';
comment on column square.t_functioninput.fk_function is 'id of function';
comment on column square.t_functioninput.input_name is 'name of input param';
comment on column square.t_functioninput.default_value is 'default value';
comment on column square.t_functioninput.ordering is 'order of input param';
comment on column square.t_functioninput.description is 'meaning';
comment on column square.t_functioninput.type_restriction is 'restriction on type';

grant select,insert,update,delete on square.t_functioninput to INTERFACE_SQUARE;
grant select,insert,update on square.t_functioninput to interface_rserver;

