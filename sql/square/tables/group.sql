create table if not exists square.t_group(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  constraint pk_group primary key (pk),
  constraint uk_group unique (name)
);

create index idx_group on square.t_group(name);

comment on column square.t_group.pk is 'unique id';
comment on column square.t_group.name is 'unique name of a group';

grant select,insert,update,delete on square.t_group to interface_square;

