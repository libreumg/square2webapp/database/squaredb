create table if not exists square.t_iomessage(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_iostack int not null,
  loglevel text not null,
  logmessage text not null,
  constraint pk_iomessage primary key (pk),
  constraint fk_iomessage_iostack foreign key (fk_iostack) references square.t_iostack(pk)
);

-- create trigger trg_bud_iomessage before update or delete on square.t_iomessage for each row execute procedure hist_square.history();

comment on column square.t_iomessage.fk_iostack is 'reference to corresponding iostack entry';
comment on column square.t_iomessage.loglevel is 'the log level; will be interpreted by Square² as the user message color';
comment on column square.t_iomessage.logmessage is 'the log message';

grant select,insert,update,delete on square.t_iomessage to INTERFACE_SQUARE;
grant insert,update on square.t_iomessage to interface_rserver;
