create table if not exists square.t_matrixcolumnexcludescale(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_scale integer not null,
  fk_matrixcolumn integer not null,
  value boolean,
  constraint pk_matrixcolumnexcludescale primary key (pk),
  constraint uk_matrixcolumnexcludescale unique (fk_scale, fk_matrixcolumn),
  constraint fk_matrixcolumnexcludescale_scale foreign key (fk_scale) references square.t_scale(pk),
  constraint fk_matrixcolumnexcludescale_matrixcolumn foreign key (fk_matrixcolumn) references square.t_matrixcolumn(pk)
);

-- create trigger trg_bud_matrixcolumnexcludescale before update or delete on square.t_matrixcolumnexcludescale for each row execute procedure hist_square.history();

comment on column square.t_matrixcolumnexcludescale.fk_matrixcolumn is 'corresponding matrix column';

grant select,insert,update,delete on square.t_matrixcolumnexcludescale to INTERFACE_SQUARE;
grant select on square.t_matrixcolumnexcludescale to INTERFACE_RSERVER;

