create table if not exists square.t_translation(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_lang integer not null,
  isocode enum_isocode not null,
  value text not null,
  constraint pk_translation primary key (pk),
  constraint uk_translation unique (fk_lang, isocode),
  constraint fk_translation_languagekey foreign key (fk_lang) references square.t_languagekey(pk)
);

comment on column square.t_translation.fk_lang is 'language key';
comment on column square.t_translation.isocode is 'iso-code for language due to iso-631';
comment on column square.t_translation.value is 'translation into referenced language';

grant select,insert,update on square.t_translation to interface_rserver;
grant select,insert,update,delete on square.t_translation to interface_square;

