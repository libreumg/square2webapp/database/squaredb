create table if not exists square.t_function(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  vignette text,
  before text,
  body text not null,
  after text,
  test text,
  activated boolean default false,
  category enum_functioncategory,
  type enum_functiontype,
  creator integer not null,
  creationdate timestamp default now() not null,
  summary text not null,
  description text not null,
  varname text not null,
  varlist boolean,
  dataframe text not null,
  vardef text not null,
  fk_privilege integer not null,
  datasource enum_datasource default 'raw' not null,
  use_intervals boolean default true not null,
  constraint pk_function primary key (pk),
  constraint uk_function unique (name),
  constraint fk_function_person foreign key (creator) references square.t_person(usnr),
  constraint fk_function_privilege foreign key (fk_privilege) references square.t_privilege(pk)
);

comment on column square.t_function.pk is 'primary key';
comment on column square.t_function.name is 'unique name of function';
comment on column square.t_function.vignette is 'vignette of an R function';
comment on column square.t_function.before is 'code to be executed before function_body';
comment on column square.t_function.body is 'code to be executed after function_before';
comment on column square.t_function.after is 'code to be executed after function_body';
comment on column square.t_function.test is 'code to be executed on testing function_body';
comment on column square.t_function.activated is 'false = deactivated (default), true = activated';
comment on column square.t_function.category is 'category of function';
comment on column square.t_function.type is 'type of function';
comment on column square.t_function.creator is 'usnr of creator';
comment on column square.t_function.creationdate is 'date of creation';
comment on column square.t_function.summary is 'summary of function';
comment on column square.t_function.description is 'documentation of function';
comment on column square.t_function.varname is 'name of variable or list of variable to be worked on';
comment on column square.t_function.varlist is 'if true, varname is a list of varnames';
comment on column square.t_function.dataframe is 'name of data frame to work with; column name of this data frame must contain function_varname or values of function_varlist';
comment on column square.t_function.vardef is 'name of meta data view v_variable to work with';
comment on column square.t_function.datasource is 'data source of variables, may be raw, raw without missings and transcoded ones';
comment on column square.t_function.use_intervals is 'this function uses interval restrictions';

grant select,insert,update on square.t_function to interface_rserver;
grant select,insert,update,delete on square.t_function to INTERFACE_SQUARE;

