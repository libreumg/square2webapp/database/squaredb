create table if not exists square.t_matrixelementparameter(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_matrixelementvalue integer not null,
  fk_functioninput integer,
  value text,
  refers_metadata boolean default false not null,
  constraint pk_matrixelementparameter primary key (pk),
  constraint uk_matrixelementparameter unique (fk_matrixelementvalue, fk_functioninput),
  constraint fk_matrixelementparameter_mev foreign key (fk_matrixelementvalue) references square.t_matrixelementvalue(pk),
  constraint fk_matrixelementparameter_fi foreign key (fk_functioninput) references square.t_functioninput(pk)
);

-- create trigger trg_bud_matrixelementparameter before update or delete on square.t_matrixelementparameter for each row execute procedure hist_square.history();

comment on column square.t_matrixelementparameter.fk_matrixelementvalue is 'unique id';
comment on column square.t_matrixelementparameter.fk_functioninput is 'reference to t_functioninput.pk';
comment on column square.t_matrixelementparameter.value is 'value of function input';
comment on column square.t_matrixelementparameter.refers_metadata is 'refers to metadata if true';

grant select,insert,update,delete on square.t_matrixelementparameter to INTERFACE_SQUARE;
grant select on square.t_matrixelementparameter to INTERFACE_RSERVER;

