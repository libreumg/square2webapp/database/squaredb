create table if not exists square.t_variableattributerole(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  inputwidget text,
  regexpattern text,
  has_primary_attribute boolean not null default false,
  has_locale boolean not null default false,
  constraint pk_variableattributerole primary key (pk),
  constraint uk_variableattributerole unique (name)
);

-- create trigger trg_bud_variableattributerole before update or delete on square.t_variableattributerole for each row execute procedure hist_square.history();

comment on column square.t_variableattributerole.pk is 'unique id';
comment on column square.t_variableattributerole.name is 'name of role';
comment on column square.t_variableattributerole.inputwidget is 'the input widget';
comment on column square.t_variableattributerole.regexpattern is 'validation support';

grant select,insert,update,delete on square.t_variableattributerole to interface_square;
grant select,insert,update,delete on square.t_variableattributerole to interface_rserver;

