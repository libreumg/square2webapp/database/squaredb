create table if not exists square.t_useroption(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_usnr int not null,
  fk_option int not null,
  value text,
  constraint pk_useroption primary key (pk),
  constraint uk_useroption unique (fk_usnr, fk_option),
  constraint fk_useroption_usnr foreign key (fk_usnr) references square.t_person(usnr),
  constraint fk_useroption_option foreign key (fk_option) references square.t_option(pk)
);

-- create trigger trg_bud_useroption before update or delete on square.t_useroption for each row execute procedure hist_square.history();

comment on column square.t_useroption.pk is 'unique id';
comment on column square.t_useroption.fk_usnr is 'reference to user';
comment on column square.t_useroption.fk_option is 'reference to option';
comment on column square.t_useroption.value is 'the concrete value of the user';

grant select,insert,update,delete on square.t_useroption to interface_square;
grant select on square.t_useroption to interface_rserver;

