create table if not exists square.t_missinglist(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  description text,
  fk_element integer not null,
  constraint pk_missinglist primary key (pk),
  constraint uk_missinglist unique (name),
  constraint fk_missinglist_element foreign key (fk_missinglist) references square.t_element(pk)
);

-- create trigger trg_bud_missinglist before update or delete on square.t_missinglist for each row execute procedure hist_square.history();

comment on column square.t_missinglist.pk is 'primary key';
comment on column square.t_missinglist.name is 'name of missing list';
comment on column square.t_missinglist.description is 'description of missing list';
comment on column square.t_missinglist.fk_element is 'reference to study group';

grant select,insert,update,delete on square.t_missinglist to INTERFACE_SQUARE;
grant select,insert,update on square.t_missinglist to interface_rserver;

