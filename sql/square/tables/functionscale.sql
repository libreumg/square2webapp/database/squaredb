create table if not exists square.t_functionscale(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_function integer not null,
  fk_scale integer not null,
  constraint pk_functionscale primary key (pk),
  constraint uk_functionscale unique (fk_function, fk_scale),
  constraint fk_functionscale_function foreign key (fk_function) references square.t_function(pk),
  constraint fk_functionscale_scale foreign key (fk_scale) references square.t_scale(pk)
);

create index idx_functionscale_function on square.t_functionscale(fk_function);

-- create trigger trg_bud_functionscale before update or delete on square.t_functionscale for each row execute procedure hist_square.history();

comment on column square.t_functionscale.pk is 'primary key';

grant select,insert,update,delete on square.t_functionscale to INTERFACE_SQUARE;
grant select,insert,update on square.t_functionscale to interface_rserver;
