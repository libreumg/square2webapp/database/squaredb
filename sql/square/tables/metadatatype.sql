create table if not exists square.t_metadatatype(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  description text,
  constraint pk_metadatatype primary key (pk),
  constraint uk_metadatatype unique (name)
);

-- create trigger trg_bud_metadatatype before update or delete on square.t_metadatatype for each row execute procedure hist_square.history();

comment on column square.t_metadatatype.pk is 'primary key';
comment on column square.t_metadatatype.name is 'name of metadata type';
comment on column square.t_metadatatype.description is 'description of metadatatype';

grant select,insert,update,delete on square.t_metadatatype to INTERFACE_SQUARE;
grant select on square.t_metadatatype to interface_rserver;

