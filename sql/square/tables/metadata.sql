create table if not exists square.t_metadata(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_element integer not null,
  fk_metadatatype integer not null,
  fk_reference_element integer not null,
  constraint pk_metadata primary key (pk),
  constraint uk_metadata unique (fk_element, fk_metadatatype),
  constraint fk_metadata_element foreign key (fk_element) references square.t_hierarchy(key_element),
  constraint fk_metadata_type foreign key (fk_metadatatype) references square.t_metadatatype(pk)
);

comment on column square.t_metadata.fk_element is 'variable that this references belong to';
comment on column square.t_metadata.fk_metadatatype is 'type of metadata that this reference belongs to';
comment on column square.t_metadata.fk_reference_element is 'variable that this metadatatype refers to';

grant select,insert,update,delete on square.t_metadata to INTERFACE_SQUARE;

