create table if not exists square.t_option(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  key text not null,
  value text,
  order_nr int not null,
  inputtype text,
  fk_lang_section_title int not null,
  admin_only boolean default false not null,
  constraint pk_option primary key (pk),
  constraint uk_option unique (key),
  constraint fk_option_lang foreign key (fk_lang_section_title) references square.t_languagekey(pk)
);

create index idx_option on square.t_option(key);

-- create trigger trg_bud_option before update or delete on square.t_option for each row execute procedure hist_square.history();

comment on column square.t_option.pk is 'unique id';
comment on column square.t_option.key is 'unique option key';
comment on column square.t_option.value is 'default value';
comment on column square.t_option.order_nr is 'order for the display of options';
comment on column square.t_option.inputtype is 'defines the input type';
comment on column square.t_option.fk_lang_section_title is 'reference to language key of section title';
comment on column square.t_option.admin_only is 'if true, only the admin can adjust this';

grant select,insert,update,delete on square.t_option to interface_square;
grant select on square.t_option to interface_rserver;

