create table if not exists square.t_login(
  lastchange timestamp default current_timestamp,
  fk_usnr integer not null,
  password text not null,
  expire_on timestamp not null,
  constraint pk_login primary key (fk_usnr),
  constraint fk_login_usnr foreign key (fk_usnr) references square.t_person(usnr)
);

comment on column square.t_login.fk_usnr is 'foreign key to the user';
comment on column square.t_login.password is 'the password hash';
comment on column square.t_login.expire_on is 'a timestamp when the user login expires';

grant select,insert,update,delete on square.t_login to INTERFACE_SQUARE;

