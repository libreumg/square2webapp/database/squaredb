create table if not exists square.t_snipplettype(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  type text not null,
  template text not null,
  fk_htmlcomponent integer not null,
  order_nr integer not null,
  constraint pk_snipplettype primary key (pk),
  constraint uk_snipplettype unique (type),
  constraint uk_snipplettype_ordernr unique (order_nr),
  constraint fk_snipplettype_htmlcomponent foreign key (fk_htmlcomponent) references square.t_htmlcomponent(pk)
);

create trigger trg_bud_snipplettype before update or delete on square.t_snipplettype for each row execute procedure hist_square.history();

comment on column square.t_snipplettype.type is 'unique identifier of type';
comment on column square.t_snipplettype.template is 'template; replace ${code} by user content';
comment on column square.t_snipplettype.fk_htmlcomponent is 'html component';
comment on column square.t_snipplettype.order_nr is 'order number';

grant select,insert,update,delete on square.t_snipplettype to INTERFACE_SQUARE;

