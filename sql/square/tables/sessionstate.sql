create table if not exists square.t_sessionstate(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_usnr integer not null,
  key text not null,
  value text,
  constraint pk_sessionstate primary key (pk),
  constraint uk_sessionstate unique (fk_usnr, key),
  constraint fk_sessionstate_usnr foreign key (fk_usnr) references square.t_person(usnr)
);

comment on column square.t_sessionstate.pk is 'primary key';
comment on column square.t_sessionstate.fk_usnr is 'foreign key to user';
comment on column square.t_sessionstate.key is 'key of this parameter';
comment on column square.t_sessionstate.value is 'value of this parameter';

grant select,insert,update,delete on square.t_sessionstate to INTERFACE_SQUARE;

