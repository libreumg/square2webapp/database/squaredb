create table if not exists square.t_maintenance(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  message text not null,
  downtime_start timestamp not null,
  downtime_end timestamp not null,
  affected_square boolean not null,
  constraint pk_maintenance primary key (pk)
);

comment on column square.t_maintenance.pk is 'unique id';
comment on column square.t_maintenance.message is 'message to be displayed on web applications';
comment on column square.t_maintenance.downtime_start is 'estimated downtime start';
comment on column square.t_maintenance.downtime_end is 'estimated downtime end';
comment on column square.t_maintenance.affected_square is 'is square affected?';

grant select,insert,update,delete on square.t_maintenance to interface_square;

