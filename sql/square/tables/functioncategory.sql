create table if not exists square.t_functioncategory(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_function integer,
  name text not null,
  description text,
  constraint pk_functioncategory primary key (pk),
  constraint uk_functioncategory unique (fk_function, name),
  constraint fk_functioncategory_function foreign key (fk_function) references square.t_function(pk)
);

create index idx_functioncategory_function on square.t_functioncategory(fk_function);

-- create trigger trg_bud_functioncategory before update or delete on square.t_functioncategory for each row execute procedure hist_square.history();

comment on column square.t_functioncategory.pk is 'primary key';
comment on column square.t_functioncategory.fk_function is 'id of function; if null, this is a category definition';
comment on column square.t_functioncategory.name is 'name of category';
comment on column square.t_functioncategory.description is 'description';

grant select,insert,update,delete on square.t_functioncategory to INTERFACE_SQUARE;
grant select,insert,update on square.t_functioncategory to interface_rserver;

