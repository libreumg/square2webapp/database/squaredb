create table if not exists square.t_snipplet(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_snipplettype integer not null,
  code text,
  markdowntext text,
  order_nr integer not null,
  fk_report integer not null,
  fk_functionoutput integer,
  fk_matrixcolumn integer,
  constraint pk_snipplet primary key (pk),
  constraint fk_snipplet_report foreign key (fk_report) references square.t_report(pk),
  constraint fk_snipplet_type foreign key (fk_snipplettype) references square.t_snipplettype(pk),
  constraint fk_snipplet_functionoutput foreign key (fk_functionoutput) references square.t_functionoutput(pk),
  constraint fk_snipplet_matrixcolumn foreign key (fk_matrixcolumn) references square.t_matrixcolumn(pk)
);

comment on column square.t_snipplet.fk_snipplettype is 'type of snipplet';
comment on column square.t_snipplet.code is 'code of snipplet';
comment on column square.t_snipplet.order_nr is 'index of snipplet in report';
comment on column square.t_snipplet.fk_report is 'reference to report';

grant select,insert,update,delete on square.t_snipplet to INTERFACE_SQUARE;
grant select,insert,update,delete on square.t_snipplet to INTERFACE_RSERVER;

