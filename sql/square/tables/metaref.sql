create table if not exists square.t_metaref(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_metadata integer not null,
  metareftype enum_metareftype not null,
  fk_element integer not null,
  constraint pk_metaref primary key (pk),
  constraint uk_metaref unique (fk_metadata, metareftype),
  constraint fk_metaref_metadata foreign key (fk_metadata) references square.t_metadata(pk),
  constraint fk_metaref_variable foreign key (fk_element) references square.t_hierarchy(key_element)
);

comment on column square.t_metaref.fk_metadata is 'reference to meta data';
comment on column square.t_metaref.metareftype is 'type of metadata reference that this belongs to';
comment on column square.t_metaref.fk_element is 'variable that this reference belongs to';

grant select,insert,update,delete on square.t_metaref to INTERFACE_SQUARE;

