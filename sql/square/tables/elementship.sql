create table if not exists square.t_elementship(
  lastchange timestamp default current_timestamp,
  fk_element integer,
  dn text not null,
  constraint uk_elementship unique (fk_element),
  constraint fk_elementship_element foreign key (fk_element) references square.t_element(pk)
);

create index idx_elementship_element on square.t_elementship(fk_element);

create trigger trg_bud_elementship before update or delete on square.t_elementship for each row execute procedure hist_square.history();

comment on column square.t_elementship.fk_element is 'reference to element id';
comment on column square.t_elementship.dn is 'unique name of SHIP';

grant select,insert,update,delete on square.t_elementship to interface_square;
grant select,insert,update,delete on square.t_elementship to interface_rserver;

