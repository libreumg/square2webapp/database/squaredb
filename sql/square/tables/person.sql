create table if not exists square.t_person(
  lastchange timestamp default current_timestamp,
  usnr integer not null,
  username text not null,
  forename text not null,
  surname text not null,
  contact jsonb,
  constraint pk_person primary key (usnr),
  constraint uk_username unique (username)
);

comment on column square.t_person.usnr is 'id of user used in database';
comment on column square.t_person.username is 'name of user used for login';

grant select,insert,update on square.t_person to interface_square;
grant select,insert,update on square.t_person to interface_rserver;

