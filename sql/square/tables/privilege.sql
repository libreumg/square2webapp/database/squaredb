create table if not exists square.t_privilege(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text,
  constraint pk_privilege primary key (pk),
  constraint uk_privilege_name unique (name)
);


grant select,insert,update,delete on square.t_privilege to interface_square;
grant select,insert,update,delete on square.t_privilege to interface_rserver;

