create table if not exists square.t_htmlcomponent(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  constraint pk_htmlcomponent primary key (pk),
  constraint uk_htmlcomponent unique (name)
);

create trigger trg_bud_htmlcomponent before update or delete on square.t_htmlcomponent for each row execute procedure hist_square.history();

comment on column square.t_htmlcomponent.name is 'unique identifier of type';

grant select on square.t_htmlcomponent to INTERFACE_SQUARE;

