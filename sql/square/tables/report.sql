create table if not exists square.t_report(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  description text,
  fk_parent integer,
  fk_latexdef integer,
  fk_functionlist integer not null,
  fk_variablegroupdef integer,
  started timestamp,
  stopped timestamp,
  runlog text,
  runsum text,
  latexcode text,
  constraint pk_report primary key (pk),
  constraint uk_report_name unique (name),
  constraint fk_report_parent foreign key (fk_parent) references square.t_report(pk),
  constraint fk_report_latexdef foreign key (fk_latexdef) references square.t_latexdef(pk),
  constraint fk_report_functionlist foreign key (fk_functionlist) references square.t_functionlist(pk),
  constraint fk_report_variablegroupdef foreign key (fk_variablegroupdef) references square.t_variablegroupdef(pk)
);

comment on column square.t_report.name is 'unique name of report';
comment on column square.t_report.description is 'optional description of report';
comment on column square.t_report.fk_parent is 'reference to report clone origin';
comment on column square.t_report.fk_latexdef is 'reference to latex style definition';
comment on column square.t_report.fk_anatemp is 'reference to analysis template';
comment on column square.t_report.fk_variablegroupdef is 'reference to variable list';
comment on column square.t_report.runlog is 'last log of run, if done';
comment on column square.t_report.runsum is 'summary of run log, if done';
comment on column square.t_report.latexcode is 'complete latex document containing snipplets and calculation results';

grant select,insert,update,delete on square.t_report to INTERFACE_SQUARE;
grant select,insert,update,delete on square.t_report to INTERFACE_RSERVER;

