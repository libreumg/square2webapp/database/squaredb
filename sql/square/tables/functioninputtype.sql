create table if not exists square.t_functioninputtype(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  fk_lang integer not null,
  constraint pk_functioninputtype primary key (pk),
  constraint uk_functioninputtype unique(name),
  constraint fk_functioninputtype_lang foreign key (fk_lang) references square.t_languagekey(pk)
);

comment on column square.t_functioninputtype.pk is 'primary key';
comment on column square.t_functioninputtype.name is 'name of input type';
comment on column square.t_functioninputtype.fk_lang is 'description of input type';

grant select,insert,update,delete on square.t_functioninputtype to INTERFACE_SQUARE;
grant select,insert,update on square.t_functioninputtype to interface_rserver;

