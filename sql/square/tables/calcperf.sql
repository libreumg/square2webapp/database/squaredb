create table if not exists square.t_calcperf(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_report integer,
  fk_function integer not null,
  var_magnitude integer,
  user_self numeric,
  sys_self numeric,
  elapsed numeric,
  user_child numeric,
  sys_child numeric,
  constraint pk_calcperf primary key (pk),
  constraint uk_calcperf unique (fk_report, fk_function),
  constraint fk_calcperf_fktn foreign key (fk_function) references square.t_function(pk),
  constraint fk_calcperf_report foreign key (fk_report) references square.t_report(pk)
);

comment on column square.t_calcperf.fk_calculation is 'reference to calculation';
comment on column square.t_calcperf.fk_function is 'reference to function';
comment on column square.t_calcperf.fk_report is 'reference to report';

grant select,delete on square.t_calcperf to interface_square;
grant select,insert,update,delete on square.t_calcperf to interface_rserver;

