create table if not exists square.t_reportdatarelease(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_report integer not null,
  fk_release integer not null,
  constraint pk_reportdatarelease primary key (pk),
  constraint uk_reportdatarelease unique (fk_report, fk_release),
  constraint fk_reportdatarelease_release foreign key (fk_release) references square.t_release(pk),
  constraint fk_reportdatarelease_report foreign key (fk_report) references square.t_report(pk)
);

-- create trigger trg_bud_reportdatarelease before update or delete on square.t_reportdatarelease for each row execute procedure hist_square.history();

comment on column square.t_reportdatarelease.fk_report is 'reference to report';
comment on column square.t_reportdatarelease.fk_release is 'reference to release';

grant select,insert,update,delete on square.t_reportdatarelease to interface_square;
grant select on square.t_reportdatarelease to interface_rserver;

