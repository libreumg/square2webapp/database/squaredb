create table if not exists square.t_studygroup(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_element int not null,
  locales jsonb default '{}'::jsonb,
  constraint pk_studygroup primary key (pk),
  constraint uk_studygroup unique (fk_element),
  constraint fk_studygroup_element foreign key (fk_element) references square.t_element(pk)
);

-- create trigger trg_bud_studygroup before update or delete on square.t_studygroup for each row execute procedure hist_square.history();


grant select,insert,update,delete on square.t_studygroup to interface_square;
grant select,insert,update,delete on square.t_studygroup to interface_rserver;

