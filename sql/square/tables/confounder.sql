create table if not exists square.t_confounder(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_element integer not null,
  fk_metadatatype integer not null,
  fk_referenced_element integer not null,
  constraint pk_confounder primary key (pk),
  constraint uk_confounder unique (fk_element, fk_metadatatype),
  constraint uk_confounder_elem unique (fk_element, f_referenced_element),
  constraint fk_confounder_element foreign key (fk_element) references square.t_element(pk),
  constraint fk_confounder_type foreign key (fk_metadatatype) references square.t_metadatatype(pk)
);

create trigger trg_bud_confounder before update or delete on square.t_confounder for each row execute procedure hist_square.history();

comment on column square.t_confounder.fk_element is 'variable that this references belong to';
comment on column square.t_confounder.fk_metadatatype is 'type of metadata that this reference belongs to';
comment on column square.t_confounder.fk_referenced_element is 'variable that this metadatatype refers to';

grant select,insert,update,delete on square.t_confounder to INTERFACE_SQUARE;
grant select,insert,update,delete on square.t_confounder to INTERFACE_RSERVER;

