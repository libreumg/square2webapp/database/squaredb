create table if not exists square.t_matrixcolumn(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  description text,
  fk_functionlist integer not null,
  fk_function integer not null,
  required_executions text,
  fk_parent integer,
  use_intervals boolean,
  order_nr integer not null,
  constraint pk_matrixcolumn primary key (pk),
  constraint uk_matrixcolumn unique (fk_functionlist, name),
  constraint fk_matrixcolumn_functionlist foreign key (fk_functionlist) references square.t_functionlist(pk),
  constraint fk_matrixcolumn_function foreign key (fk_function) references square.t_function(pk),
  constraint fk_matrixcolumn_parent foreign key (fk_parent) references square.t_matrixcolumn(pk)
);

-- create trigger trg_bud_matrixcolumn before update or delete on square.t_matrixcolumn for each row execute procedure hist_square.history();

comment on column square.t_matrixcolumn.name is 'name of function list';
comment on column square.t_matrixcolumn.description is 'description of function list';
comment on column square.t_matrixcolumn.fk_functionlist is 'corresponding function list';
comment on column square.t_matrixcolumn.fk_function is 'corresponding function';
comment on column square.t_matrixcolumn.required_executions is 'list of matrixcolumn pks that have results that are needed by this function';
comment on column square.t_matrixcolumn.use_intervals is 'use intervals';
comment on column square.t_matrixcolumn.order_nr is 'order in analysis matrix';

grant select,insert,update,delete on square.t_matrixcolumn to INTERFACE_SQUARE;
grant select on square.t_matrixcolumn to INTERFACE_RSERVER;

