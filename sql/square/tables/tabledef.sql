create table if not exists square.T_TABLEDEF(
  lastchange timestamp default CURRENT_TIMESTAMP,
  pk serial NOT NULL,
  fk_element integer NOT NULL,
  name text NOT NULL,
  schema text NOT NULL,
  constraint pk_tabledef primary key (pk) using index tablespace null,
  constraint uk_tabledef unique (fk_element, name, schema) using index tablespace null,
  constraint fk_tabledef_element foreign key (fk_element) references square.t_hierarchy(key_element)
) tablespace null;

create trigger trg_bud_TABLEDEF before update or delete on square.t_tabledef for each row execute procedure common_dd.history();


comment on column square.t_tabledef.name is 'name of the database table';
comment on column square.t_tabledef.schema is 'name of the databse schema';

grant select,insert,update,delete on square.T_TABLEDEF to INTERFACE_SQUARE;
grant select on square.T_TABLEDEF to INTERFACE_RSERVER;
grant usage on square.T_TABLEDEF_pk_seq to INTERFACE_SQUARE;

