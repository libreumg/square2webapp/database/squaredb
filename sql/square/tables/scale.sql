create table if not exists square.t_scale(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  datatypename text,
  fk_parent integer,
  constraint pk_scale primary key (pk),
  constraint uk_scale unique (name),
  constraint fk_scale_parent foreign key (fk_parent) references t_scale(pk)
);

create trigger trg_bud_scale before update or delete on square.t_scale for each row execute procedure hist_square.history();

comment on column square.t_scale.name is 'name of scale';
comment on column square.t_scale.fk_parent is 'parent scale';
comment on column square.t_scale.datatypename is 'name of dataquieR datatype';

grant select,insert,update,delete on square.t_scale to INTERFACE_SQUARE;
grant select,insert,update on square.t_scale to INTERFACE_RSERVER;
