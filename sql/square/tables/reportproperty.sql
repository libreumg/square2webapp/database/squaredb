create table if not exists square.t_reportproperty(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_report integer not null,
  fk_widgetbased integer not null,
  value jsonb,
  constraint pk_reportproperty primary key (pk),
  constraint uk_reportproperty unique (fk_report, fk_widgetbased),
  constraint fk_reportproperty_wb foreign key (fk_widgetbased) references square.t_widgetbased(pk)
);

-- create trigger trg_bud_reportproperty before update or delete on square.t_reportproperty for each row execute procedure hist_square.history();

comment on column square.t_reportproperty.fk_report is 'reference to report';
comment on column square.t_reportproperty.fk_widgetbased is 'reference to widgetbased';
comment on column square.t_reportproperty.value is 'the value';

grant select,insert,update,delete on square.t_reportproperty to interface_square;
grant select on square.t_reportproperty to interface_rserver;

