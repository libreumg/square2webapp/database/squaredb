create table if not exists square.t_functionoutputtype(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  description text not null,
  latex_before text,
  latex_after text,
  constraint pk_functionoutputtype primary key (pk),
  constraint uk_functionoutputtype unique(name)
);

comment on column square.t_functionoutputtype.pk is 'primary key';
comment on column square.t_functionoutputtype.name is 'name of output type';
comment on column square.t_functionoutputtype.description is 'description of output type';
comment on column square.t_functionoutputtype.latex_before is 'latex code to be wrapped around; this part is before';
comment on column square.t_functionoutputtype.latex_after is 'latex code to be wrapped around; this part is after';

grant select,insert,update,delete on square.t_functionoutputtype to INTERFACE_SQUARE;
grant select,insert,update on square.t_functionoutputtype to interface_rserver;

