create table if not exists square.t_study(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_element integer not null,
  start_date timestamp,
  designfeature timestamp,
  participants integer,
  constraint pk_study primary key (pk),
  constraint fk_study_element foreign key (fk_element) references square.t_element(pk)
);

create index idx_qs_study_element on square.t_study(fk_element);

comment on column square.t_study.fk_element is 'unique id';
comment on column square.t_study.start_date is 'date of start of this study';
comment on column square.t_study.designfeature is 'end date of design feature';
comment on column square.t_study.participants is 'number of participants of this study';

grant select,insert,update,delete on square.t_study to INTERFACE_SQUARE;

