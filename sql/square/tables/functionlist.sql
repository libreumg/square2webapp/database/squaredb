create table if not exists square.t_functionlist(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  name text not null,
  description text not null,
  fk_parent integer,
  constraint pk_functionlist primary key (pk),
  constraint uk_functionlist unique (name),
  constraint fk_functionlist_parent foreign key (fk_parent) references square.t_functionlist(pk)
);

-- create trigger trg_bud_functionlist before update or delete on square.t_functionlist for each row execute procedure hist_square.history();

comment on column square.t_functionlist.name is 'name of function list';
comment on column square.t_functionlist.description is 'description of function list';
comment on column square.t_functionlist.fk_parent is 'parent entry';

grant select,insert,update,delete on square.t_functionlist to INTERFACE_SQUARE;
grant select on square.t_functionlist to INTERFACE_RSERVER;

