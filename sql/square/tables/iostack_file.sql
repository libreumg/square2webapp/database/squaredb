create table if not exists square.t_iostack_file(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_iostack int not null,
  file bytea not null,
  constraint pk_iostack_file primary key (pk),
  constraint uk_iostack_file unique (fk_iostack),
  constraint fk_iostack_file_iostack foreign key(fk_iostack) references square.t_iostack(pk)
);

-- create trigger trg_bud_iostack_file before update or delete on square.t_iostack_file for each row execute procedure hist_square.history();

comment on column square.t_iostack_file.file is 'the iostack';
comment on column square.t_iostack_file.file is 'the raw file';

grant select,insert on square.t_iostack_file to interface_square;
grant select,delete on square.t_iostack_file to interface_rserver;

