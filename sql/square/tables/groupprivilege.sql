create table if not exists square.t_groupprivilege(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_privilege integer not null,
  fk_group integer not null,
  square_acl enum_accesslevel,
  constraint pk_groupprivilege primary key (pk),
  constraint uk_groupprivilege unique (fk_group, fk_privilege),
  constraint fk_groupprivilege_privilege foreign key (fk_privilege) references square.t_privilege(pk),
  constraint fk_groupprivilege_group foreign key (fk_group) references square.t_group(pk)
);

create index idx_groupprivilege_privilege on square.t_groupprivilege(fk_privilege);
create index idx_groupprivilege_group on square.t_groupprivilege(fk_group);


grant select,insert,update,delete on square.t_groupprivilege to INTERFACE_SQUARE;

