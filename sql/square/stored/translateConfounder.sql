create or replace function square.translate_confounder(refers_metadata boolean, unique_name text, default_value jsonb) returns jsonb as
$$
declare
  result jsonb;
  scalar boolean;
begin
	result := default_value;
  if refers_metadata = true then
    select jsonb_typeof(default_value) = 'string' into scalar;
    if scalar = true then
      default_value := '[' || default_value::text || ']';
    end if;
    select jsonb_agg(referenced_variable) into result
      from square.r_confounder 
      where metadatatypename in (select * from jsonb_array_elements_text(default_value))
      and variable = unique_name;
    if scalar = true then
      result := result->0;
    end if;
  end if;
  return result;
end                                                                                                                                                                  
$$ language plpgsql;

grant execute on function square.translate_confounder(boolean, text, jsonb) to interface_square;
grant execute on function square.translate_confounder(boolean, text, jsonb) to interface_rserver;
