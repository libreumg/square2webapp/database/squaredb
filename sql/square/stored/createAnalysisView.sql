create or replace function square.create_analysis_view(tablename text, schemaname text, viewname text) returns text as $$
declare
  res text;
  cols text;
  isNotFirst boolean;
  rec record;
begin
	res := 'create or replace view ' || schemaname || '.' || viewname || ' as select ';
	cols := '';
	isNotFirst := false;
  for rec in (
		select case when v.fk_element is null then c.column_name else 'v' || v.fk_element end as new_column, c.column_name, c.ordinal_position 
      from square.t_variable v
      right join information_schema.columns c on c.column_name = v.column_name
      where c.table_name = tablename 
        and c.table_schema = schemaname
        and (v.fk_element in (
          select key_element 
            from square.v_study
            left join square.t_release on t_release.fk_element = v_study.fk_element
            where location = schemaname
        ) or v.fk_element is null
      ) order by c.ordinal_position
  ) loop
    if isNotFirst then
      cols := cols || ', ';
    end if;
    cols := cols || rec.column_name || ' as ' || rec.new_column;
    isNotFirst := true;
  end loop;
  if cols = '' then
    res := '';
    raise notice 'ignoring table % because there are no such entries in the data dictionary, no ids found to be used in the view', tablename;
  else
	  res := res || cols || ' from ' || schemaname || '.' || tablename;
  end if;
  return res;
end;
$$ language plpgsql;

create or replace function square.create_analysis_view(tablename text, schemaname text) returns void security definer as $$
declare
  newView text;
  viewname text;
  stmt text;
begin
	viewname := regexp_replace(tablename, '^t_', 'v_square_');
	newView := schemaname || '.' || viewname;
  stmt := square.create_analysis_view(tablename, schemaname, viewname);
  if stmt = '' then
  	raise notice 'nothing to grant here';  
  else
    execute 'drop view if exists ' || newView;
    execute 'grant select on ' || schemaname || '.' || tablename || ' to interface_rserver, ship_import;';
    execute stmt;
    execute 'alter view ' || newView || ' owner to ship_import';
    execute 'grant select on ' || newView || ' to interface_rserver, interface_square';
    raise notice '(re)created view % for table %', newView, tablename;
  end if;
end;
$$ language plpgsql;

grant execute on function square.create_analysis_view(text, text) to interface_square;
grant execute on function square.create_analysis_view(text, text, text) to interface_square;
