create or replace function square.create_analysis_views(schemaname text) returns void security definer as $$
declare
  rec record;
  stmt text;
  newView text;
  viewname text;
begin
  for rec in (select t.table_name
              from information_schema.tables t
              full outer join information_schema.tables v on upper(v.table_name) = upper(regexp_replace(t.table_name, '^t_', 'v_square_'))
                and coalesce(v.table_schema, schemaname) = schemaname
                and coalesce(v.table_type, 'VIEW') = 'VIEW'
              where t.table_schema = schemaname 
              and t.table_type = 'BASE TABLE' 
              and upper(t.table_name) like 'T_%'
              and v.table_name is null
              ) 
  loop
    viewname := regexp_replace(rec.table_name, '^t_', 'v_square_');
    newView := schemaname || '.' || viewname;
    stmt := square.create_analysis_view(rec.table_name, schemaname, viewname);
    if stmt = '' then
  	  raise notice 'nothing to grant here';  
    else
      execute 'drop view if exists ' || newView;
      execute stmt;
      execute 'grant select on ' || schemaname || '.' || rec.table_name || ' to interface_rserver, ship_import';
      execute 'grant select on ' || newView || ' to interface_rserver, interface_square';
      execute 'alter view ' || newView || ' owner to ship_import';
      raise notice 'created view % for table %', newView, rec.table_name;
    end if;
  end loop;
end;
$$ language plpgsql;

grant execute on function square.create_analysis_views(schemaname text) to interface_square;
