create or replace function square.check_jsonb_pattern(regex text, value jsonb)
returns boolean
as $$
declare
  res boolean;
begin
  select count(regexp_match(value::text, regex)) > 0 into res;
  return res;
end;
$$ language plpgsql;

grant execute on function square.check_jsonb_pattern(text, jsonb) to interface_square, interface_rserver;
