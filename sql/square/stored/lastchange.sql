create or replace function square.lastchange() returns trigger security definer as $$
begin
  select now() into new.lastchange;
  return new;
end                                                                                                                                                                    
$$ language plpgsql;

grant execute on function square.lastchange() to interface_square, interface_rserver;
