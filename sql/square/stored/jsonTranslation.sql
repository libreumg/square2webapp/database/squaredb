create or replace function square.json_translation(langKey integer) returns text as $$
declare
  result text;
  notFirstLoop boolean;
  r record;
begin
	result := '{';
	notFirstLoop = false;
  for r in (select isocode, value from square.t_translation where fk_lang = langKey)
  loop
    if notFirstLoop = true then
      result := result || ', ';
    end if;
    result := result || '"' || r.isocode::text || '": "' || r.value::text || '"'; 
    notFirstLoop := true;
  end loop;
  result := result || '}';
  return result;
end                                                                                                                                                                    
$$ language plpgsql;

grant execute on function square.json_translation(integer) to interface_square;
