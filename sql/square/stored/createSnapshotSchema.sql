create or replace function square.create_snapshot_schema(schemaname text) returns text security definer as $$
declare
  str1 text;
  newschemaname text;
  rec record;
begin
	execute 'select to_char(now(), $1)' into str1 using 'YYYYMMDD';
	newschemaname := schemaname || '_snapshot_' || str1;
	execute 'create schema if not exists ' || newschemaname;
	execute 'grant create, usage on schema ' || newschemaname || ' to interface_square, interface_rserver';
	raise notice 'created schema % if not there before', newschemaname;
	for rec in (
		select table_name 
		from information_schema.tables 
		where table_schema = schemaname and table_type = 'BASE TABLE'
  ) loop
    str1 := newschemaname || '.' || rec.table_name;
    execute 'drop table if exists ' || str1;
    execute 'create table ' || str1 || ' as select * from ' || schemaname || '.' || rec.table_name;
    execute 'grant select on table ' || str1 || ' to interface_rserver, interface_square';
		raise notice 'transferred %.% to %', schemaname, rec.table_name, str1;
  end loop;
  return newschemaname;
end;
$$ language plpgsql;

grant execute on function square.create_snapshot_schema(text) to interface_square;
