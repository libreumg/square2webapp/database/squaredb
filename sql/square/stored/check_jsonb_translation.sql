create or replace function square.check_jsonb_translation()
returns trigger
as $$
declare
  valid boolean;
  regex text;
begin
  regex := '{(".*"\s*:\s*".*")*,*}';
  if new.translation is null then
    new.translation := '{}';
  end if;
  valid := square.check_jsonb_pattern(regex, new.translation);
  if valid = false then
    raise exception 'error on execution: the translation % does not fit to the pattern %', new.translation, regex;
  end if;
  return new;
end;
$$ language plpgsql;

grant execute on function square.check_jsonb_translation() to interface_square, interface_rserver;