create or replace function square.get_rpackage_version(rpackage text) returns text as $$
  v <- "not installed"
  if (requireNamespace(rpackage, quietly = TRUE)) {
    library(squaremetadataimporter)
    v <- get_package_version(rpackage)
  }
  return(v)
$$ language plr;
