\i sql/square/views/group.sql
\i sql/square/views/orphanedfunctions.sql
\i sql/square/views/function.sql
\i sql/square/views/functionprivs.sql
\i sql/square/views/functionreference.sql
\i sql/square/views/functionoutputtype.sql
\i sql/square/views/privilege.sql
\i sql/square/views/privileges.sql
\i sql/square/views/variablerelease.sql
\i sql/square/views/variablegrouprelease.sql
\i sql/square/views/report.sql
\i sql/square/views/reportsnipplet.sql
\i sql/square/views/functionright.sql
\i sql/square/views/login.sql
\i sql/square/views/tree.sql
\i sql/square/views/study.sql
\i sql/square/views/hierarchy.sql
\i sql/square/views/metaexp.sql
\i sql/square/views/metadata.sql
\i sql/square/views/vargroupdeps.sql
\i sql/square/views/variablegroup.sql
\i sql/square/views/variablelimits.sql
\i sql/square/views/anavarpar.sql

\i sql/square/views/R/anaparfun.sql
\i sql/square/views/R/anaparfunflag.sql
\i sql/square/views/R/anavarpar.sql
\i sql/square/views/R/calcuinterval.sql
\i sql/square/views/R/function.sql
\i sql/square/views/R/variable.sql

