create type square.enum_quality as enum (
  'blue', 
  'yellow', 
  'green',
  'gray'
);
