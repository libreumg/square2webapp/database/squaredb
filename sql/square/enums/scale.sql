create type square.enum_scale as enum (
  'text', 
  'nominal', 
  'ordinal', 
  'count', 
  'metric', 
  'datetime',
  'binary'
);
