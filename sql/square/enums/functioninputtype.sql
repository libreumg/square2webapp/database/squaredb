create type square.enum_functioninputtype as enum (
  'numeric parameter',
  'variable itself',
  'list of variables',
  'variable start',
  'variable end',
  'variable observer',
  'variable device',
  'variable reader',
  'text parameter',
  'variable other',
  'list other' 
);
