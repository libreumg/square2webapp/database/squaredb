create type square.enum_functiontype as enum (
  'inner routine',
  'wrapper',
  'latex export'
);
