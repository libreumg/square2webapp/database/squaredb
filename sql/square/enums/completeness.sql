create type square.enum_completeness as enum (
  'rolling', 
  'cleaning', 
  'final'
);
