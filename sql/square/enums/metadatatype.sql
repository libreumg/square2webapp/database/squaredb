create type square.enum_metadatatype as enum (
  'observer', 
  'reader', 
  'MTA',
  'study center',
  'intervention group'
);
