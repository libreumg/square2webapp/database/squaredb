create type square.enum_datasource as enum (
  'raw',
  'nomissing',
  'transcoded'
);
