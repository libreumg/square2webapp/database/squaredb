create type square.enum_functioncategory as enum (
  'unknown',  
  'missing values',
  'extreme values',
  'time trends',
  'observer/device/reader differences',
  'descriptive statistics'  
);
