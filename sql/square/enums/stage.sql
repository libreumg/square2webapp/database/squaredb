create type square.enum_stage as enum (
  'stage.dev_test', 
  'stage.test', 
  'stage.test_prod'
);
