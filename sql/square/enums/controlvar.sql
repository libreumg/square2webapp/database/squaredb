create type square.enum_controlvar as enum (
  'enum.controlvar.experimentee', 
  'enum.controlvar.redo', 
  'enum.controlvar.visit', 
  'enum.controlvar.observation', 
  'enum.controlvar.login', 
  'enum.controlvar.timestamp',
  'enum.controlvar.center'
);
