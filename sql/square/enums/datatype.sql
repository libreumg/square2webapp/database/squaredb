create type square.enum_datatype as enum (
  'integer',
  'string',
  'datetime',
  'float'
);
