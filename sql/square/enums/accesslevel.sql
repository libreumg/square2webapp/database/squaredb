create type square.enum_accesslevel as enum (
  'r',
  'w', 
  'x', 
  'rw', 
  'wx', 
  'rx',
  'rwx' 
);
