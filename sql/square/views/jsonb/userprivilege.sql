create or replace view square.v_jsonb_userprivilege as
select up.fk_privilege,
       jsonb_agg(jsonb_build_object(
         'username', u.username,
         'forename', u.forename,
         'surname', u.surname,
         'contact', u.contact,
         'square_acl', up.square_acl
       )) as userprivileges
from square.t_userprivilege up
left join square.t_person u on u.usnr = up.fk_usnr
group by up.fk_privilege;

grant select on square.v_jsonb_userprivilege to interface_rest;
