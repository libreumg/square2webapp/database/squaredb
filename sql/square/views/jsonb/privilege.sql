create or replace view square.v_jsonb_privilege as
select p.pk,
       vru.userprivileges,
       vrg.groupprivileges,
       p.name
from square.t_privilege p
left join square.v_jsonb_userprivilege vru on vru.fk_privilege = p.pk
left join square.v_jsonb_groupprivilege vrg on vrg.fk_privilege = p.pk
where vru.userprivileges is not null or vrg.groupprivileges is not null;

grant select on square.v_jsonb_privilege to interface_rest;
