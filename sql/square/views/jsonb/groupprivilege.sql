create or replace view square.v_jsonb_groupprivilege as
select gp.fk_privilege,
       jsonb_agg(jsonb_build_object(
         'groupname', g.name,
         'square_acl', gp.square_acl
       )) as groupprivileges
from square.t_groupprivilege gp 
left join square.t_group g on g.pk = gp.fk_group 
group by gp.fk_privilege;

grant select on square.v_jsonb_groupprivilege to interface_rest;
