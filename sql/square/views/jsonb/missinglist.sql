create or replace view square.v_jsonb_missinglist as
select ml.pk,
       jsonb_agg(jsonb_build_object('code', m.code, 
                                    'name_in_study', m.name_in_study, 
									'missingtype_name', mt.name)) as missinglist, 
	   ml.name as missinglist_name,
	   ml.description as missinglist_description,
	   e.unique_name as missinglist_study
from square.t_missing m
left join square.t_missingtype mt on mt.pk = m.fk_missingtype 
left join square.t_missinglist ml on ml.pk = m.fk_missinglist
left join square.t_element e on e.pk = ml.fk_element
group by ml.pk, ml.name, ml.description, e.unique_name;;

grant select on square.v_jsonb_missinglist to interface_rest;
