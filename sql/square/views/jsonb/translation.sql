create or replace view square.v_jsonb_translation as
select l.pk, 
       jsonb_agg(json_build_object('isocode', t.isocode, 'value', t.value)) as translation_list, 
       l.name
from square.t_translation t
left join square.t_languagekey l on l.pk = t.fk_lang
group by l.pk, l.name;

grant select on square.v_jsonb_translation to interface_rest;
