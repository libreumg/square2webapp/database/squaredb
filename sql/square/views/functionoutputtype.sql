create or replace view square.v_functionoutputtype as
select count(o.type) as reference_amount, t.pk, t.name, t.description 
from square.t_functionoutput o 
right join square.t_functionoutputtype t on t.pk = o.type
group by o.type, t.pk, t.name, t.description;

grant select on square.v_functionoutputtype to interface_square;
