create view square.v_variablerelease as
select array_agg(distinct r.pk) as release_pks,
       e.unique_name 
from square.t_variable v 
left join square.t_element e on e.pk = v.fk_element
left join information_schema.columns c on c.column_name = v.column_name
left join information_schema.tables t on t.table_name = c.table_name and t.table_schema = c.table_schema
left join square.t_release r on r.location = c.table_schema
left join square.v_study s on s.key_element = v.fk_element
where t.table_type = 'BASE TABLE'
and s.fk_element = r.fk_element
group by e.unique_name;

grant select on square.v_variablerelease to interface_square;
