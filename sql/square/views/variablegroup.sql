create view square.v_variablegroup as
select vg.pk, vg.name, vg.description, vg.fk_privilege,
       count(vge.fk_element) as elements
from square.t_variablegroup vg
left join square.t_variablegroupelement vge on vge.fk_variablegroup = vg.pk
group by 1, 2, 3, 4;

grant select on square.v_variablegroup to interface_square;
