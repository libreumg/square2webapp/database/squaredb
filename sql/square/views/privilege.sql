create view square.v_privilege as
with x(p, u, a) as (
select gp.fk_privilege, ug.fk_usnr, case gp.square_acl::text
       when 'r' then B'001'::bit(3)
       when 'w' then B'010'::bit(3)
       when 'rw' then B'011'::bit(3)
       when 'x' then B'100'::bit(3)
       when 'rx' then B'101'::bit(3)
       when 'wx' then B'110'::bit(3)
       when 'rwx' then B'111'::bit(3)
       else null end
from square.t_groupprivilege gp
left join square.t_usergroup ug on ug.fk_group = gp.fk_group
union all
select fk_privilege, fk_usnr, case square_acl::text
       when 'r' then B'001'::bit(3)
       when 'w' then B'010'::bit(3)
       when 'rw' then B'011'::bit(3)
       when 'x' then B'100'::bit(3)
       when 'rx' then B'101'::bit(3)
       when 'wx' then B'110'::bit(3)
       when 'rwx' then B'111'::bit(3)
       else null end
from square.t_userprivilege
) select fk_privilege,
         fk_usnr,
         acl,
         acl::text like '%r%' as read,
         acl::text like '%w%' as write,
         acl::text like '%x%' as execute
  from (
  select p as fk_privilege, u as fk_usnr, case bit_or(a) 
         when B'001' then 'r'::square.enum_accesslevel
         when B'010' then 'w'::square.enum_accesslevel
         when B'011' then 'rw'::square.enum_accesslevel
         when B'100' then 'x'::square.enum_accesslevel
         when B'101' then 'rx'::square.enum_accesslevel
         when B'110' then 'wx'::square.enum_accesslevel
         when B'111' then 'rwx'::square.enum_accesslevel
         else null end as acl
  from x
  where a is not null
  group by 1, 2) as outerselect;

grant select on square.v_privilege to interface_square;
