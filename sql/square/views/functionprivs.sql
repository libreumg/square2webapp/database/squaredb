create view square.v_functionprivs as
select f.pk, f.name, json_agg(json_build_object('usnr', u.fk_usnr, 'acl', u.square_acl)) as acls
from square.t_function f
left join square.t_userprivilege u on u.fk_privilege = f.fk_privilege
where u.square_acl is not null 
group by f.pk, f.name;

grant select on square.v_functionprivs to interface_square;
