create or replace view square.v_function as
select v.pk,
       v.activated,
       v.name,
       v.vignette,
       v.varlist,
       v.varname,
       v.dataframe,
       v.vardef,
       v.datasource,
       v.name || ' <- function(' || v.varname || ', ' || v.dataframe || ', ' || v.vardef || ', datasource' || (
       case when count(v.input_param) > 0 then ', ' || string_agg(v.input_param, ', ' order by v.ordering) else '' end
       ) ||') {' || chr(10) || chr(10) || '  ' || regexp_replace(v.body, '\r', '', 'g') || chr(10) || chr(10) || '}' as body_function,
       'before_' || v.name || ' <- function(' || v.varname || ', ' || v.dataframe || ', ' || v.vardef || ', datasource' || (
       case when count(v.input_param) > 0 then ', ' || jsonb_agg(v.input_param order by v.ordering)::text else '' end
       ) || ') {' || chr(10) || regexp_replace(v.before, '\r', '', 'g') || chr(10) || '}' as before_function,
       'after_' || v.name || ' <- function(' || v.varname || ', ' || v.dataframe || ', ' || v.vardef || ', datasource' || (
       case when count(v.input_param) > 0 then ', '|| jsonb_agg(v.input_param order by v.ordering)::text else '' end
       ) || ') {' || chr(10) || regexp_replace(v.after, '\r', '', 'g') || chr(10) || '}' as after_function,
       case when count(v.input_param) < 1 then null 
            else json_agg(json_build_object('input_name', v.input_name, 'usage', v.usage)) 
       end as input_definition
from (
select f.pk,
       f.activated,
       f.name, 
       f.vignette,  
       fi.input_name as input_param, 
       fi.ordering,
       fi.usage,
       f.body,
       f.before,
       f.after,
       f.test,
       f.varname,
       f.varlist,
       f.dataframe,
       f.vardef,
       f.datasource,
       fi.input_name  
from square.t_function f
left join square.t_functioninput fi on fi.fk_function = f.pk
) v
group by v.pk, v.name, v.vignette, v.body, v.before, v.after, v.test, v.activated, v.varname, v.dataframe, v.varlist, v.vardef, v.datasource
;

grant select on square.v_function to interface_square;
grant select on square.v_function to interface_rserver;
