create view square.v_group as
select g.pk, g.name, json_agg(fk_usnr) filter (where fk_usnr is not null) as users
from square.t_group g
left join square.t_usergroup on fk_group = g.pk
group by g.pk, g.name;

grant select on square.v_group to interface_square;
