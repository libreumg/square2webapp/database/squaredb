create or replace view square.v_study as
with recursive t(k, p, path, n) as (
  select pk, fk_parent, array[]::integer[], name 
  from square.t_element
  where fk_parent is null
  union all
  select pk, fk_parent, path || fk_parent, name 
  from square.t_element 
  join t on k = fk_parent
)
select k as key_element, n as name, s.fk_element, h.name as study_name, h.unique_name as study_unique_name, s.start_date, h.fk_privilege as study_privilege, p.name as studygroup_name, p.fk_privilege as studygroup_privilege, g.locales
from t, square.t_study s
left join square.t_element h on h.pk = s.fk_element
left join square.t_element p on p.pk = h.fk_parent
left join square.t_studygroup g on g.fk_element = p.pk
where s.fk_element = any(path)
union all
select s.fk_element as key_element, h.name, s.fk_element, h.name as study_name, h.unique_name as study_unique_name, s.start_date, h.fk_privilege as study_privilege, p.name as studygroup_name, p.fk_privilege as studygroup_privilege, g.locales
from square.t_study s
left join square.t_element h on h.pk = s.fk_element
left join square.t_element p on p.pk = h.fk_parent
left join square.t_studygroup g on g.fk_element = p.pk
;

grant select on square.v_study to interface_square;
grant select on square.v_study to interface_rserver;
