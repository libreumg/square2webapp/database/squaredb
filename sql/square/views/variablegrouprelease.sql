create view square.v_variablegrouprelease as
with x(fg, r) as (
  select vge.fk_variablegroup, unnest(vr.release_pks)
  from square.t_variablegroupelement vge
  left join square.t_element e on e.pk = vge.fk_element
  left join square.v_variablerelease vr on vr.unique_name = e.unique_name
  where vr.release_pks is not null
  group by 1, 2
) select fg as fk_variablegroup, 
         array_agg(r) as release_pks
from x
group by fg;

grant select on square.v_variablegrouprelease to interface_square;
