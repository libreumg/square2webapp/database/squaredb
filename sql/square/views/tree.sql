create or replace view square.v_tree as 
  with recursive q(k, p, tree) as (
    select pk, fk_parent, coalesce(name, 'element.' || pk)                                                                                                                                                  
    from square.t_element
    where fk_parent is null
    union all
    select h.pk, h.fk_parent, 
           (q.tree || '.'::text || coalesce(h.name, 'element.' || h.pk)) as tree
    from square.t_element h
    join q on q.k = h.fk_parent
  )
  select q.k as fk_element, h.fk_lang, q.tree, h.unique_name
  from q
  left join square.t_element h on h.pk = q.k;

grant select on square.v_tree to interface_square;
