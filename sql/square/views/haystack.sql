create view square.v_haystack as
with x(u, k) as (
  select e.unique_name, jsonb_object_keys(e.translation)
  from square.t_element e
), y(unique_name, needle) as (
  select e.unique_name, e.translation ->> k
  from x
  left join square.t_element e on e.unique_name = u
  union all
  select e.unique_name, e.name
  from square.t_element e
  union all
  select e.unique_name, t.value
  from square.t_element e
  left join square.t_translation t on t.fk_lang = e.fk_lang
  union all
  select e.unique_name, v.column_name
  from square.t_variable v
  left join square.t_element e on e.pk = v.fk_element
  union all
  select e.unique_name, a.value
  from square.t_variableattribute a
  left join square.t_variable v on v.fk_element = a.fk_variable 
  left join square.t_element e on e.pk = v.fk_element 
) select distinct unique_name, needle
from y
where needle is not null;

grant select on square.v_haystack to interface_square;
