create or replace view square.v_privileges as
select true as isgroup, 
       p.pk as pk,
       p.fk_group,
       g.name,
       p.fk_privilege,
       json_agg(u.fk_usnr) as users, 
       p.square_acl
from square.t_groupprivilege p 
left join t_usergroup u on u.fk_group = p.fk_group
left join t_group g on g.pk = p.fk_group
group by 1, 2, 3, 4, 5, 7
union all
select false as isgroup, 
       pk,
       null,
       null,
       fk_privilege, 
       json_build_array(fk_usnr), 
       square_acl
from square.t_userprivilege;

grant select on square.v_privileges to interface_square;
