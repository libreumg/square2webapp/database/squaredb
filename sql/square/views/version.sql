create or replace view square.v_version as
select 79 as db_version;

grant select on square.v_version to interface_rserver, interface_square;
