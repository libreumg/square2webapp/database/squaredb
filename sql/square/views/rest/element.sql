drop view if exists square.v_rest_element;
create or replace view square.v_rest_element as
select e.unique_name,
       p.unique_name as parent_unique_name,
       e.name,
       e.order_nr,
			 o.tree,
			 s.study_name,
			 s.studygroup_name,
			 e.fk_lang,
			 v.fk_element is not null as is_variable
from square.t_element e
left join square.t_variable v on v.fk_element = e.pk
left join square.t_element p on p.pk = e.fk_parent
left join square.v_tree o on o.fk_element = e.pk
left join square.v_study s on s.key_element = e.pk
order by o.tree;

grant select on square.v_rest_element to interface_rest;
grant select on square.t_element to interface_rest;
grant select on square.t_variable to interface_rest;
grant select on square.v_tree to interface_rest;
grant select on square.v_study to interface_rest;
grant select on square.t_study to interface_rest;
