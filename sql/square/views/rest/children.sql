drop view if exists square.v_rest_children;
create or replace view square.v_rest_children as
select e.unique_name,
       e.name,
       e.order_nr,
       p.unique_name as parent_unique_name,
       e.fk_lang,
       count(c.pk) < 1 as leaf,
       coalesce(array_agg(distinct c.unique_name) filter (where c.unique_name is not null), array[]::text[]) as children
from square.t_element e
left join square.t_element p on p.pk = e.fk_parent
left join square.t_element c on c.fk_parent = e.pk
group by e.unique_name, e.name, e.order_nr, p.unique_name, e.fk_lang;

grant select on square.v_rest_children to interface_rest;
grant select on square.t_element to interface_rest;
