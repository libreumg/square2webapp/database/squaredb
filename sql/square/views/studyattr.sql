create or replace view square.v_studyattr as 
  select e.unique_name, 
         at.name, 
         sa.value, 
         at.entity, 
         at.meta, 
         wb.key, 
         wb.value as default_value, 
         wb.type_restriction, 
         wb.order_nr as widget_order_nr,
         fit.name as inputtype_name, 
         jsonb_object_agg(l.isocode, l.value) as translation
  from square.t_studyattr sa
  left join square.t_study s on s.pk = sa.fk_study
  left join square.t_element e on e.pk = s.fk_element
  left join square.t_attrtype at on at.pk = sa.fk_attrtype
  left join square.t_widgetbased wb on wb.pk = at.fk_widgetbased
  left join square.t_functioninputtype fit on fit.pk = wb.fk_functioninputtype
  left join square.t_translation l on l.fk_lang = fit.fk_lang
  group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
;

grant select on square.v_tree to interface_square;
