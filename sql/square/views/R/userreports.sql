create view square.r_user_reports as 
select distinct r.pk as id, r.name, r.description, 
                          v.name as vargroup,
                          f.name as functions,
                          extract(epoch from r.stopped - r.started)::int as durations,
                          l.name as release,
                          p1.username as groupuser,
                          p2.username as directuser
          from square.t_report r
          left join square.t_reportdatarelease rrl on rrl.fk_report = r.pk
          left join square.t_release l on l.pk = rrl.fk_release
          left join square.t_functionlist f on f.pk = r.fk_functionlist
          left join square.t_variablegroup v on v.pk = r.fk_variablegroup
          left join square.t_userprivilege up on up.fk_privilege = v.fk_privilege
          left join square.t_groupprivilege gp on gp.fk_privilege = v.fk_privilege
          left join square.t_usergroup ug on ug.fk_group = gp.fk_group
          left join square.t_person p1 on p1.usnr = ug.fk_usnr
          left join square.t_person p2 on p2.usnr = up.fk_usnr
          where r.stopped is not null;
         
grant select on square.r_user_reports to interface_rserver;
