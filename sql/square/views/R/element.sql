create view square.r_element as
select e.pk as fk_element, 
       e.unique_name, 
       v.fk_element is not null as is_variable
from square.t_element e 
left join square.t_variable v on v.fk_element = e.pk ;

grant select on square.r_element to interface_rserver;
