create view square.r_reportconfounder as
select rep.pk as fk_report, e.unique_name, m.name, r.unique_name as referenced_unique_name
from square.t_confounder c
left join square.t_element e on e.pk = c.fk_element
left join square.t_variablegroupelement vge on vge.fk_element = c.fk_element
left join square.t_report rep on rep.fk_variablegroup = vge.fk_variablegroup 
left join square.t_element r on r.pk = c.fk_referenced_element 
left join square.t_metadatatype m on m.pk = c.fk_metadatatype
where rep.pk is not null;

grant select on square.r_reportconfounder to interface_rserver;