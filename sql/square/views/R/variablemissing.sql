create view square.r_variablemissing as
with x(e, d) as (
  select fk_element,  min(distance) as min
  from square.r_variablemissinglist
  group by fk_element
), vml(fk_element, fk_missinglist) as (
  select vml.fk_element, vml.fk_missinglist
  from square.r_variablemissinglist vml
  join x on vml.fk_element = x.e and vml.distance = x.d
) select r.pk as fk_report,
         m.code as "CODE_VALUE",
         m.name_in_study as "CODE_LABEL",
         upper(t.name) as "CODE_CLASS",
         e.unique_name as resp_vars
from square.t_report r
left join square.t_variablegroupelement vge on vge.fk_variablegroup = r.fk_variablegroup
left join square.t_element e on e.pk = vge.fk_element
left join vml on vml.fk_element = vge.fk_element
left join square.t_missing m on m.fk_missinglist = vml.fk_missinglist and m.code is not null
left join square.t_missingtype t on t.pk = m.fk_missingtype
where m.code is not null
group by r.pk, m.code, m.name_in_study, t.name, e.unique_name;

grant select on square.r_variablemissing to interface_rserver;
