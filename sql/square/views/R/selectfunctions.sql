create view square.r_selectfunctions as
select r.pk as fk_report,
       m.name as alias, 
       m.fk_function, 
       f.name
from square.t_matrixcolumn m
left join square.t_function f on f.pk = m.fk_function
left join square.t_report r on r.fk_functionlist = m.fk_functionlist;

grant select on square.r_selectfunctions to interface_rserver;
