create view square.r_reportproperty as
select rp.fk_report, wb.key, coalesce(rp.value, wb.value) as value
from square.t_widgetbased wb
left join square.t_reportproperty rp on rp.fk_widgetbased = wb.pk;

grant select on square.r_reportproperty to interface_rserver;
