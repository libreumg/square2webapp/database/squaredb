drop view square.r_release;
create view square.r_release as
select r.pk,
       r.name,
       r.location,
       'v'::text || r.mergecolumn as mergecolumn,
       v.column_name as merge_column
from square.t_release r
left join square.t_variable v on v.fk_element = r.mergecolumn;

grant select on square.r_release to interface_rserver;
