create view square.r_datacolumndef as
select c.table_schema, c.table_name, c.column_name 
from information_schema.columns c
left join information_schema.tables t
  on t.table_schema = c.table_schema 
  and t.table_name = c.table_name
where t.table_type = 'BASE TABLE';

grant select on square.r_datacolumndef to interface_rserver;
