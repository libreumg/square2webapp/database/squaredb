create or replace view square.r_matrixcandidate as
select r.pk as fk_report, 
       mc.pk as fk_matrixcolumn, 
       vge.pk as fk_variablegroupelement,
       v.fk_element, 
       f.name as function_name, 
       mc.name as matrixcolumn_name, 
       e.unique_name, 
       mces.value as excluded
from square.t_report r
left join square.t_variablegroupelement vge on vge.fk_variablegroup = r.fk_variablegroup
left join square.t_element e on e.pk = vge.fk_element
left join square.t_variable v on v.fk_element = e.pk
left join square.t_scale s on s.datatypename = v.datatype::text
left join square.t_functionscale fs on fs.fk_scale = s.pk
left join square.t_function f on f.pk = fs.fk_function
left join square.t_matrixcolumn mc on mc.fk_function = f.pk and mc.fk_functionlist = r.fk_functionlist
left join square.t_matrixcolumnexcludescale mces on mces.fk_matrixcolumn = mc.pk and fs.fk_scale = mces.fk_scale
where mc.pk is not null
group by (1, 2, 3, 4, 5, 6, 7, 8);

grant select on square.r_matrixcandidate to interface_rserver;
grant select on square.r_matrixcandidate to interface_square;
