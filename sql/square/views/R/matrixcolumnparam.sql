drop view if exists square.r_matrixcolumnparam;
create or replace view square.r_matrixcolumnparam as 
select fi.input_name, 
       case when mcp.value is null
       then null 
       else jsonb_build_object('value', mcp.value, 'refers_metadata', mcp.refers_metadata, 'type_name', fit.name)
       end as value, 
       fi.ordering,
       mc.name as alias,
       r.pk as fk_report,
       fi.type_restriction
from square.t_matrixcolumnparameter mcp
left join square.t_matrixcolumn mc on mc.pk = mcp.fk_matrixcolumn
left join square.t_report r on r.fk_functionlist = mc.fk_functionlist and r.pk = mcp.fk_report
left join square.t_functioninput fi on fi.pk = mcp.fk_functioninput
left join square.t_functioninputtype fit on fit.pk = fi.fk_type;

grant select on square.r_matrixcolumnparam to interface_rserver;
