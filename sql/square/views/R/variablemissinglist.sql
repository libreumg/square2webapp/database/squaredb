create view square.r_variablemissinglist as
with recursive x(k, p, m, s, fu, u, fk) as (
  select e.pk, e.fk_parent, e.fk_missinglist, 1 as score, e.unique_name, e.unique_name, v.fk_element
  from square.t_variable v
  left join square.t_element e on e.pk = v.fk_element
  where v.fk_element in (
    select fk_element
    from square.t_variablegroupelement)
  union all
  select e.pk, e.fk_parent, e.fk_missinglist, x.s + 1 as score, e.unique_name, x.u, x.fk
  from square.t_element e
  inner join x on x.p = e.pk
) select x.u as unique_name, x.m as fk_missinglist, x.fk as fk_element, x.fu as found_in_unique_name, x.s as distance
from x
where (fu, s) in (
  select fu, min(s) 
  from x 
  group by fu)
and m is not null;

grant select on square.r_variablemissinglist to interface_square;
grant select on square.r_variablemissinglist to interface_rserver;
