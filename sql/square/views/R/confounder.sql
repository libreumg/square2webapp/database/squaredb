create or replace view square.r_confounder as
select e.unique_name as variable, 
       m.name as metadatatypename, 
       r.unique_name as referenced_variable
from square.t_confounder c
left join square.t_element e on e.pk = c.fk_element
left join square.t_metadatatype m on m.pk = c.fk_metadatatype
left join square.t_element r on r.pk = c.fk_referenced_element;

grant select on square.r_confounder to interface_rserver;
