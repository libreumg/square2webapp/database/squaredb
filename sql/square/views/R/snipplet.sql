create or replace view square.r_snipplet as
SELECT t.type,
    s.fk_report,
    s.order_nr,
    s.fk_matrixcolumn,
    s.fk_functionoutput,
    s.markdowntext,
    mc.name as alias,
    fo.name as output_name
   FROM square.t_snipplet s
     LEFT JOIN square.t_snipplettype t ON t.pk = s.fk_snipplettype
     left join square.t_matrixcolumn mc on mc.pk = s.fk_matrixcolumn
     left join square.t_functionoutput fo on fo.pk = s.fk_functionoutput;

grant select on square.r_snipplet to interface_rserver;
