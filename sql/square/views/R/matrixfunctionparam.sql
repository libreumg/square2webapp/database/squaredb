drop view if exists square.r_matrixfunctionparam;
create or replace view square.r_matrixfunctionparam as 
select fi.input_name, 
       case when fi.default_value is null
       then null 
       else jsonb_build_object('value', fi.default_value, 'refers_metadata', fi.refers_metadata, 'type_name', fit.name)
       end as value, 
       fi.ordering,
       f.pk as fk_function,
       f.name as function_name,
       fi.type_restriction
from square.t_functioninput fi
left join square.t_functioninputtype fit on fit.pk = fi.fk_type
left join square.t_function f on f.pk = fi.fk_function;

grant select on square.r_matrixfunctionparam to interface_rserver;
