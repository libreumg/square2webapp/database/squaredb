create view square.r_reportvarorder as
select r.pk as fk_report, e.unique_name, vge.varorder
from square.t_report r
left join square.t_variablegroupelement vge on vge.fk_variablegroup = r.fk_variablegroup
left join square.t_element e on e.pk = vge.fk_element;

grant select on square.r_reportvarorder to interface_rserver;
