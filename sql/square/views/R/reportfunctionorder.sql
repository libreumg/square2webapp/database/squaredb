create view square.r_reportfunctionorder as
select r.pk as fk_report, mc.name, mc.order_nr
from square.t_report r
left join square.t_matrixcolumn mc on mc.fk_functionlist = r.fk_functionlist;

grant select on square.r_reportfunctionorder to interface_rserver;
