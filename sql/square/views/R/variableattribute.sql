create or replace view square.r_variableattribute as
with x(fk_element, fk_variablegroup) as (
  select v.fk_element, vge.fk_variablegroup
  from square.t_variable v
  left join square.t_variablegroupelement vge on vge.fk_element = v.fk_element 
  union all
  select distinct c.fk_referenced_element, vge.fk_variablegroup
  from square.t_confounder c
  left join square.t_variablegroupelement vge on vge.fk_element = c.fk_element
) select distinct e.unique_name, 
                  va.name, 
                  va.value, 
                  var.name as role, 
                  va.is_primary_attribute, 
                  va.locale, 
                  x.fk_variablegroup
from x
left join square.t_element e on e.pk = x.fk_element
left join square.t_variableattribute va on va.fk_variable = x.fk_element
left join square.t_variableattributerole var on var.pk = va.fk_role
;

grant select on square.r_variableattribute to interface_rserver;