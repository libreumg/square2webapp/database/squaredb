create view square.r_matrixcell as
select r.pk as fk_report, e.unique_name, mc.name as alias, mev.pk is not null as checked
from square.t_report r
left join square.t_variablegroupelement vge on vge.fk_variablegroup = r.fk_variablegroup
left join square.t_element e on e.pk = vge.fk_element
left join square.t_matrixcolumn mc on mc.fk_functionlist = r.fk_functionlist
full join square.t_matrixelementvalue mev on mev.fk_variablegroupelement = vge.pk 
                                          and mev.fk_matrixcolumn  = mc.pk
                                          and mev.fk_report = r.pk
order by vge.varorder, mc.order_nr;

grant select on square.r_matrixcell to interface_rserver;
