create view square.r_functionalias as 
select r.pk as fk_report, mc.name
from square.t_matrixcolumn mc
left join square.t_function f on f.pk = mc.fk_function
left join square.t_report r on r.fk_functionlist = mc.fk_functionlist
where f.varlist = true;

grant select on square.r_functionalias to interface_rserver;
