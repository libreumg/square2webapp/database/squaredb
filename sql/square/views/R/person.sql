create view square.r_person as
select usnr, username
from square.t_person;

grant select on square.r_person to interface_rserver;
