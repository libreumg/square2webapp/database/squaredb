drop view square.r_missing;
create view square.r_missing as
select e.unique_name, m.code, m.name_in_study, t.name as category, vge.fk_variablegroup
from t_element e
left join t_variablegroupelement vge on vge.fk_element = e.pk
left join t_missing m on m.fk_missinglist = e.fk_missinglist 
left join t_missingtype t on t.pk = m.fk_missingtype 
where m.code is not null 
group by e.unique_name, m.code, m.name_in_study, t.name, vge.fk_variablegroup;

grant select on square.r_missing to interface_rserver;
