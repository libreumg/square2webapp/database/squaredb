create or replace view square.r_matrixcolumnscale as
select mc.pk as fk_matrixcolumn,
       mc.name as matrixcolumn_name,
       s.name as scale_name
from square.t_matrixcolumn mc
left join square.t_functionscale fs on fs.fk_function = mc.fk_function
left join square.t_scale s on s.pk = fs.fk_scale
where s.name is not null
except
select mc.pk as fk_matrixcolumn,
       mc.name as matrixcolumn_name,
       s.name as scale_name
from square.t_matrixcolumn mc
left join square.t_matrixcolumnexcludescale mces on mces.fk_matrixcolumn = mc.pk
left join square.t_scale s on s.pk = mces.fk_scale
where value != true;

grant select on square.r_matrixcolumnscale to interface_rserver;
