drop view if exists square.r_matrixparentparam;
create or replace view square.r_matrixparentparam as 
select fi.input_name, 
       case when pmcp.value is null
       then null 
       else jsonb_build_object('value', pmcp.value, 'refers_metadata', pmcp.refers_metadata, 'type_name', fit.name)
       end as value, 
       fi.ordering,
       mc.name as alias,
       r.pk as fk_report,
       fi.type_restriction
from square.t_matrixcolumnparameter pmcp
left join square.t_matrixcolumn mc on mc.pk = pmcp.fk_matrixcolumn
left join square.t_report p on p.fk_functionlist = mc.fk_functionlist and p.pk is null
left join square.t_report r on r.fk_functionlist = mc.fk_functionlist and r.pk is not null
left join square.t_functioninput fi on fi.pk = pmcp.fk_functioninput
left join square.t_functioninputtype fit on fit.pk = fi.fk_type
where pmcp.fk_report is null;

grant select on square.r_matrixparentparam to interface_rserver;
