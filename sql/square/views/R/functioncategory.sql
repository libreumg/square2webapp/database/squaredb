create view square.r_functioncategory as
select r.pk as fk_report, f.name, c.name as category
from square.t_matrixcolumn m
left join square.t_function f on f.pk = m.fk_function
left join square.t_report r on r.fk_functionlist = m.fk_functionlist
left join square.t_functioncategory c on c.fk_function = f.pk;

grant select on square.r_functioncategory to interface_rserver;
