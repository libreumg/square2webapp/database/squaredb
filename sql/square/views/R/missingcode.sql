create view square.r_missingcode as
select r.pk as fk_report, m.code as "CODE_VALUE", m.name_in_study as "CODE_LABEL", t.name as category
from square.t_report r
left join square.t_variablegroupelement vge on vge.fk_variablegroup = r.fk_variablegroup
left join (
  with x(e, d) as (
    select fk_element, min(distance)
    from square.r_variablemissinglist
    group by fk_element
  ) select r.fk_element, r.fk_missinglist
  from square.r_variablemissinglist r
  inner join x on r.fk_element = x.e and r.distance = x.d
) vml on vml.fk_element = vge.fk_element
left join square.t_missing m on m.fk_missinglist = vml.fk_missinglist and m.code is not null
left join square.t_missingtype t on t.pk = m.fk_missingtype
where m.code is not null 
group by r.pk, m.code, m.name_in_study, t.name;

grant select on square.r_missingcode to interface_rserver;
