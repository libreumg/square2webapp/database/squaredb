create view square.r_reportstatus as
select r.pk as fk_report,
       r.name as name,
       r.description as description,
       vg.name as variable_group,
       vg.description as variable_group_desc,
       fl.name as function_selection,
       fl.description as function_selection_desc,
       r.started as started,
       r.stopped as stopped,
       r.runlog as runlog,
       jsonb_agg(rel.name) as releases,
       jsonb_agg(jsonb_build_object('from', rp.date_from, 'until', rp.date_until)) as periods
from square.t_report r 
left join square.t_reportdatarelease rdr on r.pk = rdr.fk_report 
left join square.t_release rel on rdr.fk_release = rel.pk
left join square.r_reportperiod rp on rp.fk_report = r.pk 
left join square.t_variablegroup vg on vg.pk = r.fk_variablegroup 
left join square.t_functionlist fl on fl.pk = r.fk_functionlist 
where vg.pk is not null
group by r.pk, r.name, r.description, vg.name, vg.description, fl.name, fl.description, 
         r.started, r.stopped, r.runlog;

grant select on square.r_reportstatus to interface_rserver;
