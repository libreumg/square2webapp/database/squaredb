drop view if exists square.r_matrixelementparam;
create or replace view square.r_matrixelementparam as 
select fi.input_name, 
       case when mep.value is null
       then null 
       else jsonb_build_object('value', mep.value, 'refers_metadata', mep.refers_metadata, 'type_name', fit.name)
       end as value, 
       fi.ordering,
       mc.name as alias,
       e.unique_name,
       r.pk as fk_report,
       fi.type_restriction
from square.t_matrixelementparameter mep
left join square.t_matrixelementvalue mev on mev.pk = mep.fk_matrixelementvalue
left join square.t_matrixcolumn mc on mc.pk = mev.fk_matrixcolumn
left join square.t_variablegroupelement vge on vge.pk = mev.fk_variablegroupelement
left join square.t_element e on e.pk = vge.fk_element
left join square.t_report r on r.fk_variablegroup = vge.fk_variablegroup and r.fk_functionlist = mc.fk_functionlist
left join square.t_functioninput fi on fi.pk = mep.fk_functioninput
left join square.t_functioninputtype fit on fit.pk = fi.fk_type;

grant select on square.r_matrixelementparam to interface_rserver;
