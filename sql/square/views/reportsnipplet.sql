create or replace view square.v_reportsnipplet as
select r.pk as report, r.name, 
       s.order_nr, s.pk as snipplet_id, s.markdowntext, s.fk_functionoutput, s.fk_matrixcolumn,
       t.pk as snipplettype_id, t.type, t.template
from square.t_report r
inner join square.t_snipplet s on s.fk_report = r.pk
inner join square.t_snipplettype t on t.pk = s.fk_snipplettype
;

grant select on square.v_reportsnipplet to interface_square;
