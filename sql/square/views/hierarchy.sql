create view square.v_hierarchy as
select h.pk,
       h.unique_name,
       t.tree,
       s.unique_name as study, 
       p.unique_name as parent
from square.t_element h
left join square.v_study st on st.key_element = h.pk
left join square.t_element s on s.pk = st.fk_element
left join square.t_element p on p.pk = h.fk_parent
left join square.v_tree t on t.fk_element = h.pk;

grant select on square.v_hierarchy to interface_square;
