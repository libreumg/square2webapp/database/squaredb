create or replace view square.v_functionreference as
select 	h1.name as function_name, 
        h1.pk as function_pk, 
        h2.name as reference_name, 
        h2.pk as reference_pk,
        h2.fk_privilege as reference_privilege
from square.t_functionreference r
left join square.t_function h1 on h1.pk = r.fk_function
left join square.t_function h2 on h2.pk = r.required_function
;

grant select on square.v_functionreference to interface_square;
