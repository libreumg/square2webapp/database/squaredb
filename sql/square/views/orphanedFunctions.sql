create or replace view square.v_orphaned_functions as 
select r.pk, r.name, r.description, array_agg(distinct p.fk_function) as needed_function, array_agg(distinct p2.fk_function) as used_function
from square.t_report r
left join t_matrixelementvalue a on a.fk_report = r.pk
left join t_matrixcolumn p on p.fk_functionlist = r.fk_functionlist and p.required_executions is null
left join t_matrixcolumn p2 on p2.pk = a.fk_matrixcolumn
where r.fk_parent is not null
group by 1, 2, 3;

grant select on square.v_orphanedfunctions to interface_square;
