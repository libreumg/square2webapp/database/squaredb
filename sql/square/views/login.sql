create or replace view square.v_login as
with x(p, e, u) as (
  select password, expire_on, fk_usnr
  from square.t_login
)
select username, forename, surname, p as password, usnr, e as expire_on
from square.t_person
left join x on u = usnr
where e > current_timestamp
;

grant select on square.v_login to interface_square;
