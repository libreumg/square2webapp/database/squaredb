drop view if exists square.v_report;

create view square.v_report as
select r.pk, 
       r.name, 
       r.description,
       r.started,
       r.stopped,
       r.fk_parent, 
       r.fk_latexdef, 
       r.fk_functionlist, 
       r.fk_variablegroup,
			 r.lastchange,
       json_agg(json_build_object('usnr', u.fk_usnr, 'acl', u.square_acl)) as vargroup_acl 
from square.t_report r
left join square.t_variablegroup d on d.pk = r.fk_variablegroup
left join square.t_userprivilege u on u.fk_privilege = d.fk_privilege
where u.square_acl is not null
group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10;

grant select on square.v_report to interface_square;
