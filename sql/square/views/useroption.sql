create or replace view square.v_useroption as 
with t(fk_lang, translation) as (
  select fk_lang, jsonb_object_agg(isocode, value)
  from square.t_translation
  group by fk_lang
) select p.usnr, o.key, coalesce(u.value, o.value) as value, o.order_nr, o.inputtype, o.admin_only,
         t.translation
from square.v_login p
full join square.t_option o on true
left join square.t_useroption u on u.fk_usnr = p.usnr and u.fk_option = o.pk
left join t on t.fk_lang = o.fk_lang_section_title;

grant select on square.v_useroption to interface_square;
grant select on square.v_useroption to interface_rserver;
