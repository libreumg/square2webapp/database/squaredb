create or replace view square.v_confounder as
with s(e,t,re) as (
	select d.fk_element, t.name, d.fk_referenced_element
	from square.t_variable v
	left join square.t_confounder d on d.fk_element = v.fk_element
	left join square.t_metadatatype t on t.pk = d.fk_metadatatype
	where d.fk_metadatatype is not null
	group by d.fk_element, t.name, d.fk_referenced_element
) select e as fk_element, jsonb_object_agg(t, re) as metareference 
  from s
  group by e;

grant select on square.v_confounder to interface_rserver;
