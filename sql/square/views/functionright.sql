create or replace view square.v_functionright as
select f.name,
  f.pk,
  f.fk_privilege,
  f.activated,
  f.summary,
  f.creator,
  p.forename as creator_forename,
  p.surname as creator_surname,
  u.forename as user_forename,
  u.surname as user_surname,
  u.usnr as user_usnr,
  r.square_acl as user_access_level
from square.t_function f
inner join square.t_person p on f.creator = p.usnr
left outer join square.v_privileges r on f.fk_privilege = r.fk_privilege
left outer join square.t_person u on r.users::text like '%' || u.usnr || '%'
;

grant select on square.v_functionright to interface_square;
