create table if not exists squareout.t_calculationresult(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_report integer not null,
  fk_matrixelementvalue integer,
  fk_matrixcolumn integer not null,
  fk_functionoutput integer,
  rdata text,
  result text,
  result_converter enum_resultconverter not null,
  result_annotation text,
  input_checksum text not null,
  constraint pk_calculationresult primary key (pk),
  constraint fk_calculationresult_matrixelementvalue foreign key (fk_matrixelementvalue) references square.t_matrixelementvalue(pk),
  constraint fk_calculationresult_functionoutput foreign key (fk_functionoutput) references square.t_functionoutput(pk),
  constraint fk_calculationresult_matrixcolumn foreign key (fk_matrixcolumn) references square.t_matrixcolumn(pk),
  constraint fk_calculationresult_report foreign key (fk_report) references square.t_report(pk)
);

create index idx_calculationresult_report on squareout.t_calculationresult(fk_report);

create unique index idxu_calculationresult_variable on squareout.t_calculationresult (fk_report, fk_matrixcolumn, fk_functionoutput, fk_matrixelementvalue) where fk_matrixelementvalue is not null and fk_functionoutput is not null;
create unique index idxu_calculationresult_variablelist on squareout.t_calculationresult (fk_report, fk_matrixcolumn, fk_functionoutput) where fk_matrixelementvalue is null and fk_functionoutput is not null;
create unique index idxu_calculationresult_variablelisterror on squareout.t_calculationresult (fk_report, fk_matrixcolumn) where fk_matrixelementvalue is null and fk_functionoutput is null;
create unique index idxu_calculationresult_variableerror on squareout.t_calculationresult (fk_report, fk_matrixcolumn, fk_matrixelementvalue) where fk_matrixelementvalue is not null and fk_functionoutput is null;

create trigger trg_bu_calculationresult before update on squareout.t_calculationresult for each row execute procedure square.lastchange();

comment on column squareout.t_calculationresult.fk_report is 'corresponding report';
comment on column squareout.t_calculationresult.fk_matrixelementvalue is 'corresponding analysis matrix variable';
comment on column squareout.t_calculationresult.fk_matrixcolumn is 'corresponding R function';
comment on column squareout.t_calculationresult.fk_functionoutput is 'corresponding funciton output';
comment on column squareout.t_calculationresult.rdata is 'result as R data object';
comment on column squareout.t_calculationresult.result is 'result as text';
comment on column squareout.t_calculationresult.result_converter is 'base64, markdown or text';
comment on column squareout.t_calculationresult.result_annotation is 'annotation for results';
comment on column squareout.t_calculationresult.input_checksum is 'checksum of input to determine calculation needs';

grant select,insert,update,delete on squareout.t_calculationresult to INTERFACE_SQUARE;
grant select,insert,update,delete on squareout.t_calculationresult to INTERFACE_RSERVER;

