create table if not exists squareout.t_reportdocument(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_report integer not null,
  outputformat text not null,
  filename text not null,
  raw_file bytea not null,
  constraint pk_reportdocument primary key (pk),
  constraint fk_reportdocument_report foreign key (fk_report) references square.t_report(pk)
);

create index idx_reportdocument_report on squareout.t_reportdocument(fk_report);

-- create trigger trg_bud_reportdocument before update or delete on squareout.t_reportdocument for each row execute procedure hist_squareout.history();

comment on column squareout.t_reportdocument.fk_report is 'corresponding report';
comment on column squareout.t_reportdocument.outputformat is 'output format';
comment on column squareout.t_reportdocument.filename is 'relative file name (without path)';
comment on column squareout.t_reportdocument.raw_file is 'the base64 encoded file content';

grant select,insert,update,delete on squareout.t_reportdocument to INTERFACE_SQUARE;
grant select,insert,update,delete on squareout.t_reportdocument to INTERFACE_RSERVER;

