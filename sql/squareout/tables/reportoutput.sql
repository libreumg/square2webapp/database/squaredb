create table if not exists squareout.t_reportoutput(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_report integer not null,
  latexcode text not null,
  pdf text,
  checksum text,
  constraint pk_reportoutput primary key (pk),
  constraint uk_reportoutput unique (fk_report, checksum),
  constraint fk_reportoutput_report foreign key (fk_report) references square.t_report(pk)
);

create index idx_reportoutput_report on squareout.t_reportoutput(fk_report);

create trigger trg_bu_reportoutput before update on squareout.t_reportoutput for each row execute procedure square.lastchange();

create function squareout.fktn_biu_reportoutput() returns trigger as $$
begin
	NEW.checksum := md5(NEW.latexcode);
	return NEW;
end; 
$$ language plpgsql;

create trigger trg_biu_reportoutput 
before insert or update on squareout.t_reportoutput
for each row execute procedure squareout.fktn_biu_reportoutput();

comment on column squareout.t_reportoutput.fk_report is 'corresponding report';
comment on column squareout.t_reportoutput.latexcode is 'latex code';
comment on column squareout.t_reportoutput.pdf is 'pdf content';
comment on column squareout.t_reportoutput.checksum is 'md5 of latex code';

grant select,insert,update,delete on squareout.t_reportoutput to INTERFACE_SQUARE;
grant select,insert,update,delete on squareout.t_reportoutput to INTERFACE_RSERVER;

