create table if not exists squareout.t_functionpkginfo(
  lastchange timestamp default current_timestamp,
  pk int generated always as identity not null,
  fk_function integer not null,
  package text not null,
  packageversion text,
  importdate timestamp not null,
  constraint pk_functionpkginfo primary key (pk),
  constraint uk_functionpkginfo unique (fk_function, package),
  constraint fk_functionpkginfo_function foreign key (fk_function) references square.t_function(pk)
);

create index idx_functionpkginfo_function on squareout.t_functionpkginfo(fk_function);

-- create trigger trg_bud_functionpkginfo before update or delete on squareout.t_functionpkginfo for each row execute procedure hist_squareout.history();

comment on column squareout.t_functionpkginfo.pk is 'primary key';
comment on column squareout.t_functionpkginfo.fk_function is 'id of function';
comment on column squareout.t_functionpkginfo.package is 'name of package';
comment on column squareout.t_functionpkginfo.packageversion is 'version of the package';
comment on column squareout.t_functionpkginfo.importdate is 'date of the import';

grant select,insert,update,delete on squareout.t_functionpkginfo to interface_rserver;

