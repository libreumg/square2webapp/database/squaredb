create view square.r_functionoutput as
select fo.pk as fk_functionoutput,
       fo.fk_function,
       fo.name as functionoutput_name,
       fo.description as functionoutput_description,
       fot.pk as fk_functionoutputtype,
       fot.name as functionoutputtype_name,
       fot.description as functionoutputtype_description
from square.t_functionoutput fo
left join square.t_functionoutputtype fot on fot.pk = fo.type;

grant select on square.r_functionoutput to interface_rserver;
