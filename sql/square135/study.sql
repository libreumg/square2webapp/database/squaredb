drop view square.r_study;
create view square.r_study as
select key_element, name, fk_element, study_name, start_date, studygroup_name
from square.v_study;

grant select on square.r_study to interface_rserver;
