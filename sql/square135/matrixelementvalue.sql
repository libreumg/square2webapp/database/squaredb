create view square.r_matrixelementvalue as
select mev.pk,
       mev.fk_report,
       mev.fk_matrixcolumn,
       vge.varorder,
       vge.fk_element,
    	 e.unique_name,
    	 v.column_name
from square.t_matrixelementvalue mev
left join square.t_variablegroupelement vge on vge.pk = mev.fk_variablegroupelement
left join square.t_element e on e.pk = vge.fk_element
left join square.t_variable v on v.fk_element = vge.fk_element;

grant select on square.r_matrixelementvalue to interface_rserver;
