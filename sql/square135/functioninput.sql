create view square.r_functioninput as
select fi.pk, 
       fi.fk_function, 
       fi.input_name, 
       fi.default_value, 
       fi.ordering, 
       fi.description, 
       fit.pk as fk_functioninputtype, 
       fit.name as inputtype_name, 
       fit.fk_lang
from square.t_functioninput fi
left join square.t_functioninputtype fit on fit.pk = fi.fk_type;

grant select on square.r_functioninput to interface_rserver;
