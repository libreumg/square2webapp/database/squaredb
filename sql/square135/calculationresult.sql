create view square.r_calculationresult as
select 
  lastchange,
  pk,
  fk_matrixelementvalue,
  fk_matrixcolumn,
  fk_report,
  fk_functionoutput,
  rdata,
  result,
  result_converter,
  result_annotation,
  input_checksum
from squareout.t_calculationresult;

create or replace function square.insert_calculationresult() returns trigger as $$ 
begin
	insert into squareout.t_calculationresult(
	    fk_matrixelementvalue,
      fk_matrixcolumn,
      fk_report,
      fk_functionoutput,
      rdata,
      result,
      result_converter,
      result_annotation ,
      input_checksum) values (
      new.fk_matrixelementvalue,
      new.fk_matrixcolumn,
      new.fk_report,
      new.fk_functionoutput,
      new.rdata,
      new.result,
      new.result_converter,
      new.result_annotation,
      new.input_checksum);
	return new;
end $$ language plpgsql;

create or replace function square.update_calculationresult() returns trigger as $$ 
begin
	update squareout.t_calculationresult
  set fk_matrixelementvalue = new.fk_matrixelementvalue,
      fk_matrixcolumn = new.fk_matrixcolumn,
      fk_report = new.fk_report,
      fk_functionoutput = new.fk_functionoutput,
      rdata = new.rdata,
      result = new.result,
      result_converter = new.result_converter,
      result_annotation = new.result_annotation,
      input_checksum = new.input_checksum
  where pk = new.pk;
	return new;
end $$ language plpgsql;

create function square.delete_calculationresult() returns trigger as $$ 
begin
	delete from squareout.t_calculationresult
  where pk = old.pk;
	return old;
end $$ language plpgsql;

create trigger trg_ioi_calculationresult
instead of insert on square.r_calculationresult
for each row execute procedure square.insert_calculationresult();

create trigger trg_iou_calculationresult
instead of update on square.r_calculationresult
for each row execute procedure square.update_calculationresult();

create trigger trg_iod_calculationresult
instead of delete on square.r_calculationresult
for each row execute procedure square.delete_calculationresult();

grant select,insert,update,delete on square.r_calculationresult to interface_rserver;
