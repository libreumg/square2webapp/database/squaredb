create view square135.r_release as
select pk,
       name,
       location,
       'v'::text || mergecolumn as mergecolumn
from square.t_release;

grant select on square135.r_release to interface_rserver;
