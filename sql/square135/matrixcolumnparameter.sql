create view square.r_matrixcolumnparameter as
select fi.input_name as name,
       coalesce(mcp.value, fi.default_value) as value,
       mcp.fk_matrixcolumn
from square.t_matrixcolumnparameter mcp
left join square.t_matrixcolumn mc on mc.pk = mcp.fk_matrixcolumn 
left join square.t_functioninput fi on fi.pk = mcp.fk_functioninput and fi.fk_function = mc.fk_function;

grant select on square.r_matrixcolumnparameter to interface_rserver;
