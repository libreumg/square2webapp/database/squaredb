drop view square.r_reportperiod;

create view square.r_reportperiod as 
select i.fk_report, i.date_from, i.date_until
from square.t_reportperiod i
left join square.t_report r on r.pk = i.fk_report
;

grant select on square.r_reportperiod to interface_rserver;
