drop view square.r_report;
create view square.r_report as
select pk, 
       fk_functionlist, 
       name, 
       description, 
       fk_variablegroup, 
       started, 
       stopped, 
       runlog
from square.t_report
where fk_parent is not null;

create function square.update_report() returns trigger as $$
begin
	update square.t_report
  set started = new.started,
      stopped = new.stopped,
      runlog = new.runlog
  where pk = new.pk;
	return new;
end $$ language plpgsql;

create trigger trg_iou_report 
instead of update 
on square.r_report
for each row 
execute procedure square.update_report();

grant select,insert,update,delete on square.r_report to interface_rserver;
