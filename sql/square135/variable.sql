drop view if exists square.r_variable;
create view square.r_variable as
select v.fk_element as key_element,
       h.name,
       v.column_name,
       v.var_order,
       s.name as fk_scale,
       v.refcat,
       v.eventcat,
       v.missinglist,
       v.jumplist,
       v.recode,
       v.limits,
       m.metareference,
       v.varlabel,
       v.varshortlabel,
       v.valuelist,
       v.control_type is not null as technical,
       v.control_type,
       square.json_translation(h.fk_lang) as description,
       h.unique_name
from square.t_variable v
left join square.t_scale s on s.pk = v.fk_scale
left join square.t_element h on h.pk = v.fk_element
left join square.v_confounder m on m.fk_element = v.fk_element;

grant select on square.r_variable to interface_rserver;
grant select on square.r_variable to interface_square;
