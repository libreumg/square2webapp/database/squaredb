create view square.r_matrixcolumn as
select mc.pk as fk_matrixcolumn, 
       mc.fk_functionlist, 
       mc.fk_function, 
       mc.required_executions, 
       case f.use_intervals
        when false then false
        when true  then coalesce(mc.use_intervals, true)
        else null
       end as use_intervals,
       mc.name as matrixcolumn_name,
       f.name as function_name
from square.t_matrixcolumn mc
left join square.t_function f on f.pk = mc.fk_function;

grant select on square.r_matrixcolumn to interface_rserver;
