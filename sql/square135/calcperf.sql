create view square.r_calcperf as
select fk_report,
       fk_function,
       var_magnitude,
       user_self,
       sys_self,
       elapsed,
       user_child,
       sys_child
from square.t_calcperf;

create function square.insert_calcperf() returns trigger as $$ 
begin
	insert into square.t_calcperf(
	    fk_report,
      fk_function,
      var_magnitude,
      user_self,
      sys_self,
      elapsed,
      user_child,
      sys_child) values (
      new.fk_report,
      new.fk_function,
      new.var_magnitude,
      new.user_self,
      new.sys_self,
      new.elapsed,
      new.user_child,
      new.sys_child);
	return new;
end $$ language plpgsql;

create or replace function square.update_calcperf() returns trigger as $$
begin
	update square.t_calcperf
  set var_magnitude = new.var_magnitude,
      user_self = new.user_self,
      sys_self = new.sys_self,
      elapsed = new.elapsed,
      user_child = new.user_child,
      sys_child = new.sys_child
  where fk_report = new.fk_report
    and fk_function = new.fk_function;
	return new;
end $$ language plpgsql;

create trigger trg_ioi_calcperf
instead of insert on square.r_calcperf
for each row execute procedure square.insert_calcperf();

create trigger trg_iou_calcperf
instead of update on square.r_calcperf
for each row execute procedure square.update_calcperf();

grant select, insert, update on square.r_calcperf to interface_rserver;
