drop view square.r_matrixelementparameter;
create view square.r_matrixelementparameter as
select mep.pk as pk_matrixelementparameter, 
       mep.fk_matrixelementvalue, 
       vge.fk_element, 
       fi.input_name,    
       case mcp.fixed
         when 'template'::enum_anadef
           then coalesce(nullif(mcp.value, ''), nullif(fi.default_value, ''))
         else 
           coalesce(nullif(mep.value, ''), nullif(mcp.value, ''), nullif(fi.default_value, '')) 
       end as value, 
       fi.ordering,
       fit.name as typename
from square.t_matrixelementparameter mep
left join square.t_matrixelementvalue mev on mev.pk = mep.fk_matrixelementvalue
left join square.t_variablegroupelement vge on vge.pk = mev.fk_variablegroupelement
left join square.t_matrixcolumn mc on mc.pk = mev.fk_matrixcolumn
left join square.t_functioninput fi on fi.fk_function = mc.fk_function
left join square.t_functioninputtype fit on fit.pk = fi.fk_type
left join square.t_matrixcolumnparameter mcp on mcp.fk_functioninput = fi.pk and mcp.fk_matrixcolumn = mc.pk 
where mep.fk_functioninput = fi.pk
group by 1, 2, 3, 4, 5, 6, 7;

grant select on square.r_matrixelementparameter to interface_rserver;
