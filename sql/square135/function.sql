drop view square.r_function;
create or replace view square.r_function as
select f.pk as fk_function,
	     f.activated,
	     f.name as function_name,
	     f.body,
	     f.before,
	     f.after,
	     f.test,
	     f.varname,
	     f.varlist,
	     f.dataframe,
	     f.vardef,
	     f.lastchange,
	     f.summary,
	     f.description,
       p.forename,
       p.surname,
       p.username,
       p.usnr,
       p.contact,
       string_agg(c.name, ',') as categories
   FROM square.t_function f
     LEFT JOIN square.t_person p ON p.usnr = f.creator
     left join square.t_functioncategory c on c.fk_function = f.pk
   group by
    f.pk, p.usnr;

grant select on square.r_function to interface_square;
grant select on square.r_function to interface_rserver;
