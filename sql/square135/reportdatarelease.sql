create view square.r_reportdatarelease as
select pk, fk_report, fk_release
from square.t_reportdatarelease;

grant select on square.r_reportdatarelease to interface_rserver;
