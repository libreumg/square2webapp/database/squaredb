drop view if exists square.r_snipplet;

create or replace function square.decode_snippletcode(s text, p int) returns text as $$
declare i int;
begin
	if s = null then
	  return null;
	end if;
	case p
	  when 1 then
	    i := position(':' in s);
	    if i < 1 then
	      return null;
	    else 
	      return substring(s, 1, i - 1);
	    end if;
	  when 2 then
	    i := position(':' in s);
	    if i < 1 then
	      return null;
	    else
	      return substring(s, i + 1);
	    end if;
	  when 3 then
	    return s;
	else
	  return s;
	end case;
end 
$$ language plpgsql;

create view square.r_snipplet as
select t.type, 
       s.fk_report, 
       s.order_nr, 
       square.decode_snippletcode(s.code, 1)::int as fk_matrixcolumn, 
       square.decode_snippletcode(s.code, 2)::int as fk_functionoutput, 
       square.decode_snippletcode(s.code, 3) as markdowntext
from square.t_snipplet s 
left join square.t_snipplettype t on t.pk = s.fk_snipplettype;

grant select on square.r_snipplet to interface_rserver;
