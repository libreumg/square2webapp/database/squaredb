create view square.r_variablegroupelement as
select pk,
       fk_variablegroup,
       fk_element,
       varorder
from square.t_variablegroupelement;

grant select on square.r_variablegroupelement to interface_rserver;
