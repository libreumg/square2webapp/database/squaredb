create view square.r_functionreference as
select pk, fk_function, required_function
from square.t_functionreference;

grant select on square.r_functionreference to interface_rserver;
