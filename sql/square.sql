\i sql/square/schema.sql
\i sql/square/enums.sql
set search_path = square
\i sql/square/stored.sql
\i sql/square/tables.sql
\i sql/square/views.sql

\i sql/hist_square/schema.sql
\i sql/hist_square/stored.sql
