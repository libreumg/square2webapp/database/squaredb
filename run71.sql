alter table square.t_confounder add constraint uk_confounder_elem unique (fk_element, fk_referenced_element);

drop view square.v_metaexp;
drop view square.v_variablelimits;
drop function square.update_variablelimits();
drop view square.r_variable;

\i sql/square/views/R/variable.sql

alter table square.t_variable drop column refcat;
alter table square.t_variable drop column eventcat;
alter table square.t_variable drop column missinglist;
alter table square.t_variable drop column jumplist;
alter table square.t_variable drop column recode;
alter table square.t_variable drop column varlabel;
alter table square.t_variable drop column varshortlabel;
alter table square.t_variable drop column limits;
