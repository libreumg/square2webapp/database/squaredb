\i sql/appconfig/schema.sql
\i sql/appconfig/enums/locale.sql
\i sql/appconfig/enums/category.sql
\i sql/appconfig/enums/role.sql
\i sql/appconfig/tables/menuitem.sql
\i sql/appconfig/tables/translation.sql
\i sql/appconfig/tables/menurole.sql
\i sql/appconfig/views/menuitem.sql

insert into appconfig.t_menuitem (order_nr, url, id) values (1, '#', 'studyoverview');
insert into appconfig.t_menuitem (order_nr, url, id) values (2, '#', 'studydesign');
insert into appconfig.t_menuitem (order_nr, url, id) values (3, '/SQuaReS', 'variableselection');
insert into appconfig.t_menuitem (order_nr, url, id) values (4, '#', 'datamanagement');
insert into appconfig.t_menuitem (order_nr, url, id) values (5, '#', 'statistic');
insert into appconfig.t_menuitem (order_nr, url, id) values (6, '#', 'functionselection');
insert into appconfig.t_menuitem (order_nr, url, id) values (7, '/SQuaReV', 'qualityreport');

insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (1, 'de', 'name', 'Studienübersicht');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (1, 'en', 'name', 'study overview');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (1, 'ro', 'name', 'Prezentare generală a studiului');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (2, 'de', 'name', 'Studienstruktur');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (2, 'en', 'name', 'study structure');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (2, 'ro', 'name', 'Structura studiului');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (3, 'de', 'name', 'Variablenauswahl');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (3, 'en', 'name', 'variable selection');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (3, 'ro', 'name', 'Grup variabil');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (4, 'de', 'name', 'Datenmanagement');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (4, 'en', 'name', 'data management');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (4, 'ro', 'name', 'Management de date');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (5, 'de', 'name', 'Statistik');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (5, 'en', 'name', 'statistics');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (5, 'ro', 'name', 'statistici');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (6, 'de', 'name', 'Funktionsauswahl');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (6, 'en', 'name', 'function selection');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (6, 'ro', 'name', 'Selectarea funcției');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (7, 'de', 'name', 'Qualitätsberichte');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (7, 'en', 'name', 'quality reports');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (7, 'ro', 'name', 'Rapoarte de calitate');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (1, 'pl', 'name', 'Przegląd badań');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (2, 'pl', 'name', 'Struktura badania');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (3, 'pl', 'name', 'Wybór zmiennej');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (4, 'pl', 'name', 'Zarządzanie danymi');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (5, 'pl', 'name', 'Statystyki');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (6, 'pl', 'name', 'Wybór funkcji');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (7, 'pl', 'name', 'Sprawozdania dotyczące jakości');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (1, 'es', 'name', 'Descripción general del estudio');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (2, 'es', 'name', 'Estructura del estudio');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (3, 'es', 'name', 'Selección de variables');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (4, 'es', 'name', 'Gestión de datos');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (5, 'es', 'name', 'Estadística');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (6, 'es', 'name', 'Selección de funciones');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (7, 'es', 'name', 'Reportes de calidad');

insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (1, 'en', 'description', 'Manage studies and their study waves');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (2, 'en', 'description', 'Manage a data collection''s structure, its data dictionary (metadata) and its process variable assignments');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (3, 'en', 'description', 'Select variables to analyse in one report');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (4, 'en', 'description', 'Define data releases and configure how data releases are accessed');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (5, 'en', 'description', 'Manage data quality indicator functions (create or install)');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (6, 'en', 'description', 'Select and pre-configure indicator functions for reports');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (7, 'en', 'description', 'Use pipeline to generate reports for assessing the data quality');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (1, 'es', 'description', 'Gestionar estudios y cohortes');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (2, 'es', 'description', 'Gestionar la estructura de los estudios y sus variables de proceso');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (3, 'es', 'description', 'Seleccionar variables para analizar');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (4, 'es', 'description', 'Gestionar y manipular los datos publicados');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (5, 'es', 'description', 'Cómputo de análisis estadísticos');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (6, 'es', 'description', 'Seleccionar y definir funciones de interés');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (7, 'es', 'description', 'Pipeline para evaluar la calidad de los datos');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (1, 'de', 'description', 'Studien und Studienwellen bearbeiten');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (2, 'de', 'description', 'Untersuchungsbereiche, Data Dictionaries (Metadaten) und Zuordnungen von Prozessvariablen bearbeiten');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (3, 'de', 'description', 'Variablen zusammenstellen, aus denen ein Bericht generiert werden kann -- Dabei können auch studienübergreifend Variablen gruppiert gewählt werden.');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (4, 'de', 'description', 'Metadaten zu Daten-Releases bearbeiten und den Datenzugriff auf Releases konfigurieren');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (5, 'de', 'description', 'DQ-Indikatorfunktionen anlegen oder installieren');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (6, 'de', 'description', 'Funktionen auswählen und ihren Aufruf vorkonfigurieren');
insert into appconfig.t_translation (fk_menuitem, locale, category, value) values (7, 'de', 'description', 'Pipeline zur Erzeugung von Berichten zur Beurteilung der Datenqualität nutzen');

insert into appconfig.t_menurole (fk_menuitem, rolename) values (1, 'studyadmin');
insert into appconfig.t_menurole (fk_menuitem, rolename) values (2, 'study');
insert into appconfig.t_menurole (fk_menuitem, rolename) values (3, 'vargroup');
insert into appconfig.t_menurole (fk_menuitem, rolename) values (4, 'datamanagement');
insert into appconfig.t_menurole (fk_menuitem, rolename) values (5, 'statistic');
insert into appconfig.t_menurole (fk_menuitem, rolename) values (5, 'statisticadmin');
insert into appconfig.t_menurole (fk_menuitem, rolename) values (6, 'analysis');
insert into appconfig.t_menurole (fk_menuitem, rolename) values (7, 'reportreader');
insert into appconfig.t_menurole (fk_menuitem, rolename) values (7, 'reportwriter');
insert into appconfig.t_menurole (fk_menuitem, rolename) values (7, 'reportbuilder');
insert into appconfig.t_menurole (fk_menuitem, rolename) values (7, 'reportdesigner');

