#!/bin/bash

REV=`git rev-parse --verify --short HEAD`
NOW=`date +%Y%m%d%H%M`
BN=`basename "$0" | sed 's/\.sh//g'`

read -p "SHIP_ADMIN [henkej]: " SHIP_ADMIN
SHIP_ADMIN="${SHIP_ADMIN:=henkej}"
read -p "SERVER [localhost]: " SERVER
SERVER="${SERVER:=localhost}"
read -p "SHIP_DB [dbppsquare]: " SHIP_DB
SHIP_DB="${SHIP_DB:=dbppsquare}"
read -p "PORT [5432]: " PORT
PORT="${PORT:=5432}"

LOGPATTERN="$BN.$REV.$SHIP_DB.$NOW.log"
if [ "$SERVER" == "localhost" ]
then
  LOGPATTERN="/dev/null"
fi

read -p "LOGFILE [$LOGPATTERN]: " LOGFILE
LOGFILE="${LOGFILE:=$LOGPATTERN}"

echo "---------------------------------------------"
echo "SHIP_ADMIN   = $SHIP_ADMIN" | tee $LOGFILE
echo "SERVER       = $SERVER" | tee -a $LOGFILE
echo "SHIP_DB      = $SHIP_DB" | tee -a $LOGFILE
echo "PORT         = $PORT" | tee -a $LOGFILE
echo "LOGFILE      = $LOGFILE" | tee -a $LOGFILE

read -p "all ok? cancel by strg+c" DUMMY | tee -a $LOGFILE

if [ "$SERVER" == "localhost" ]
then
  rlwrap psql $SHIP_DB -U $SHIP_ADMIN -p $PORT -c "\i $BN.sql" 2>&1 | tee -a $LOGFILE
else
  rlwrap psql $SHIP_DB -h $SERVER -U $SHIP_ADMIN -p $PORT -c "\i $BN.sql" 2>&1 | tee -a $LOGFILE
fi
